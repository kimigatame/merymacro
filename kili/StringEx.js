#title="Stringオブジェクト拡張"
////////////////////////////////////////////////////////////////////////////////
//■概要:
//Stringオブジェクト拡張。
////////////////////////////////////////////////////////////////////////////////

if(!("startsWith" in String.prototype)){
	/**
	* 先頭にマッチするかどうか判定する。
	* @param {string|RegExp} seg マッチ文字列
	* @param {boolean} [ignoreCase=false] 大文字小文字を無視する
	* @return {boolean}
	*/
		String.prototype.startsWith=function(seg,ignoreCase){
		if(seg instanceof RegExp){
			return new RegExp(seg.source.head("^"),ignoreCase||seg.ignoreCase?"i":"").test(this);
		}
		else{
			return ignoreCase?this.toLowerCase().indexOf(seg.toLowerCase())==0:this.indexOf(seg)==0;
		}
	}
}
if(!("endsWith" in String.prototype)){
	/**
	* 末尾にマッチするかどうか判定する。
	* @param {string|RegExp} seg マッチ文字列
	* @param {boolean} [ignoreCase=false] 大文字小文字を無視する
	* @return {boolean}
	*/
		String.prototype.endsWith=function(seg,ignoreCase){
		if(seg instanceof RegExp){
			return new RegExp(seg.source.tailing("$"),ignoreCase||seg.ignoreCase?"i":"").test(this);
		}
		else{
			var t=ignoreCase?this.toLowerCase().lastIndexOf(seg.toLowerCase()):this.lastIndexOf(seg);
			return t>=0&&t+seg.length==this.length;
		}
	}
}
if(!("enclosed" in String.prototype)){
	/**
	* 文字列で囲まれているかどうか判定する。
	* @param {string} pre 先頭の文字列
	* @param {string} [post] 末尾の文字列。省略すればpreと同じ
	* @param {boolean} [ignoreCase=false] 大文字小文字を無視する
	* @return {boolean}
	*/
	String.prototype.enclosed=function(pre,post,ignoreCase){
		return this.startsWith(pre,ignoreCase)&&this.endsWith(post||pre,ignoreCase);
	}
}
if(!("enclose" in String.prototype)){
	/**
	* 指定の文字列で囲む。
	* @param {string} pre 先頭の文字列
	* @param {string} [post] 末尾の文字列。省略すればpreと同じ
	* @return {string}
	*/
	String.prototype.enclose=function(pre,post){
		return pre+this+(post||pre);
	}
}
if(!("disclose" in String.prototype)){
	/**
	* 囲みを削除する。
	* @param {string} pre 先頭の文字列
	* @param {string} [post] 末尾の文字列。省略すればpreと同じ
	* @return {string}
	*/
	String.prototype.disclose=function(pre,post){
		return this.unhead(pre).untail(post||pre);
	}
}
if(!("dQuot" in String.prototype)){
	/**
	* 「"」で囲む。
	* @return {string}
	* 
	* @deprecated dquote()を使用すること
	*/
	String.prototype.dQuot=function(){
		return this.dquote();
	}
}
if(!("dquote" in String.prototype)){
	/**
	* 「"」で囲む。
	* @return {string}
	*/
	String.prototype.dquote=function(){
		return this.enclose('"');
	}
}
if(!("sQuot" in String.prototype)){
	/**
	* 「'」で囲む。
	* @return {string}
	* 
	* @deprecated quote()を使用すること
	*/
	String.prototype.sQuot=function(){
		return this.quote();
	}
}
if(!("quote" in String.prototype)){
	/**
	* 「'」で囲む。
	* @return {string}
	*/
	String.prototype.quote=function(){
		return this.enclose("'");
	}
}
if(!("crlf" in String.prototype)){
	/**
	* 改行コードを変換する。
	* @param {number} [type=0] 0ならCRLFへ、1ならCRへ、2ならLFへ変換
	* @return {string}
	*/
	String.prototype.crlf=function(type){
		return this.replace(/\u000d?\u000a|\u000d/g,type==1?"\u000d":type?"\u000a":"\u000d\u000a");
	}
}
if(!("insert" in String.prototype)){
	/**
	* 文字列を挿入する。
	* @param {string} seg 挿入する文字列
	* @param {!number} pos 挿入位置
	* @return {string}
	*/
	String.prototype.insert=function(seg,pos){
		return this.substring(0,pos)+seg+this.substr(pos);
	}
}
if(!("repeat" in String.prototype)){
	/**
	* 文字列を繰り返す。
	* @param {number} count 繰り返し回数
	* @return {string}
	*/
	String.prototype.repeat=function(count){
		var result="";
		for(count*=1;n>0;count>>>=1,result+=result){
			if(count&1){
				result+=this;
			}
		}
		return result;
	}
}
if(!("cut" in String.prototype)){
	/**
	* 指定の長さで分割する。
	* @param {number} first 最初の分割長。secundが無い場合、正の数ならばその長さで全体を分割する。負の数ならば分割しない
	* @param {...number|boolean} [secund=false] 2番目以降の分割長。負の数ならばその分遡る。
	* 	booleanが現れた時点で分割を終了し、trueならば残りを出力しない、falseならば残りを1つの要素として出力する
	* @return {Array.<string>}
	* 
	* @example
	* //return [ab,cd,ef,gh]
	* "abcdefgh".cut(2);
	* //return [a,bc,def]
	* "abcdefgh".cut(1,2,3,true);
	* //return [a,abc,cdefgh]
	* "abcdefgh".cut(1,-2,3,-1);
	*/
	String.prototype.cut=function(first,secund){
		var arr=[];
		if(arguments.length==1&&arguments[0]>0){
			for(var i=0;i<this.length;i+=arguments[0]){
				arr.push(this.substr(i,arguments[0]));
			}
		}else{
			var j=0;
			var len=0;
			for(var i=0;i<arguments.length;j=Math.max(j+len,0),i++){
				if(arguments[i]===false){
					break;
				}
				if(arguments[i]===true){
					j=this.length;
					break;
				}
				len=parseInt(arguments[i]);
				if(isNaN(len)){
					len=0;
					continue;
				}
				if(len>=0){
					arr.push(this.substr(j,len));
				}
			}
			if(j<this.length){
				arr.push(this.substr(j));
			}
		}
		return arr;
	}
}
if(!("escape" in String.prototype)){
	/**
	* 改行など空白文字をエスケープする。
	* <ul>
	* <li>\n</li>
	* <li>\r</li>
	* <li>\f</li>
	* <li>\t</li>
	* <li>\v</li>
	* <li>\\</li>
	* </ul>
	* @return {string}
	*/
	String.prototype.escape=(function(){
		var hash={"\n":"\\n","\r":"\\r","\f":"\\f","\t":"\\t","\v":"\\v","\\":"\\\\"}
		return function(){
			return this.replace(/[\n\r\f\t\v\\]/g,function($1){
				return hash[$1];
			});
		}
	})();
}
if(!("unescape" in String.prototype)){
	/**
	* エスケープされた改行など空白文字を戻す。
	* <ul>
	* <li>\n</li>
	* <li>\r</li>
	* <li>\f</li>
	* <li>\t</li>
	* <li>\v</li>
	* <li>\\</li>
	* </ul>
	* @return {string}
	*/
	String.prototype.unescape=(function(){
		var hash={"\\n":"\n","\\r":"\r","\\f":"\f","\\t":"\t","\\v":"\v","\\\\":"\\"}
		return function(){
			return this.replace(/(\\)+?[nrftv\\]/g,function($1,$2,$3){
				return ($2.length%2)?hash[$1]:$1;
			});
		}
	})();
}
if(!("escapeRegExp" in String.prototype)){
	/**
	* 正規表現で使用される文字をエスケープする。
	* <ul>
	* <li>\\</li>
	* <li>*</li>
	* <li>+</li>
	* <li>?</li>
	* <li>|</li>
	* <li>{</li>
	* <li>}</li>
	* <li>[</li>
	* <li>]</li>
	* <li>(</li>
	* <li>)</li>
	* <li>^</li>
	* <li>$</li>
	* <li>.</li>
	* <li>-</li>
	* </ul>
	* @return {string}
	*/
	String.prototype.escapeRegExp=(function(){
		var hash={"\\":"\\\\","*":"\\*","+":"\\+","?":"\\?","|":"\\|","{":"\\{","}":"\\}","[":"\\[","]":"\\]","(":"\\(",")":"\\)","^":"\\^","$":"\\$",".":"\\.","-":"\\-"};
		return function(){
			return this.replace(/[\\\*\+\?\|\{\}\[\]\(\)\^\$\.]/g,function($1){
				return hash[$1];
			});
		}
	})();
}
if(!("unescapeRegExp" in String.prototype)){
	/**
	* エスケープされた正規表現で使用される文字を戻す。
	* <ul>
	* <li>\\</li>
	* <li>*</li>
	* <li>+</li>
	* <li>?</li>
	* <li>|</li>
	* <li>{</li>
	* <li>}</li>
	* <li>[</li>
	* <li>]</li>
	* <li>(</li>
	* <li>)</li>
	* <li>^</li>
	* <li>$</li>
	* <li>.</li>
	* <li>-</li>
	* </ul>
	* @return {string}
	*/
	String.prototype.unescapeRegExp=(function(){
		var hash={"\\\\":"\\","\\*":"*","\\+":"+","\\?":"?","\\|":"|","\\{":"{","\\}":"}","\\[":"[","\\]":"]","\\(":"(","\\)":")","\\^":"^","\\$":"$","\\.":".","\\-":"-"};
		return function(){
			return this.replace(/\\[\\\*\+\?\|\{\}\[\]\(\)\^\$\.]/g,function($1){
				return hash[$1];
			});
		}
	})();
}
if(!("heading" in String.prototype)){
	/**
	* 指定文字列で始まっていない場合、先頭に追加する。
	* @param {string} seg マッチ文字列
	* @param {boolean} [replace=false] 追加ではなく置換する
	* @param {number} [count=1] 置換する文字数
	* @return {string}
	* 
	* @deprecated head()を使用すること
	*/
	String.prototype.heading=function(seg,replace,count){
		return this.head(seg,replace,count);
	}
}
if(!("head" in String.prototype)){
	/**
	* 指定文字列で始まっていない場合、先頭に追加する。
	* @param {string} seg マッチ文字列
	* @param {boolean} [replace=false] 追加ではなく置換する
	* @param {number} [count=1] 置換する文字数
	* @return {string}
	*/
	String.prototype.head=function(seg,replace,count){
		return this.startsWith(seg)?this:seg+(replace?this.slice(count||1):this);
	}
}
if(!("tailing" in String.prototype)){
	/**
	* 指定文字列で終わっていない場合、末尾に追加する。
	* @param {string} seg マッチ文字列
	* @param {boolean} [replace=false] 追加ではなく置換する
	* @param {number} [count=1] 置換する文字数
	* @return {string}
	* 
	* @deprecated tail()を使用すること
	*/
	String.prototype.tailing=function(seg,replace,count){
		return this.tail(seg,replace,count);
	}
}
if(!("tail" in String.prototype)){
	/**
	* 指定文字列で終わっていない場合、末尾に追加する。
	* @param {string} seg マッチ文字列
	* @param {boolean} [replace=false] 追加ではなく置換する
	* @param {number} [count=1] 置換する文字数
	* @return {string}
	*/
	String.prototype.tail=function(seg,replace,count){
		return this.endsWith(seg)?this:(replace?this.slice(0,-(count||1)):this)+seg;
	}
}
if(!("disheading" in String.prototype)){
	/**
	* 指定文字列で始まっている場合、削除する。
	* @param {string} seg マッチ文字列
	* @param {boolean} [replace=false] 削除ではなく置換する
	* @param {string} str 置換する文字列。
	* @return {string}
	* 
	* @deprecated unhead()を使用すること
	*/
	String.prototype.disheading=function(seg,replace,str){
		return this.unhead(seg,replace,str);
	}
}
if(!("unhead" in String.prototype)){
	/**
	* 指定文字列で始まっている場合、削除する。
	* @param {string} seg マッチ文字列
	* @param {boolean} [replace=false] 削除ではなく置換する
	* @param {string} str 置換する文字列。
	* @return {string}
	*/
	String.prototype.unhead=function(seg,replace,str){
		return this.startsWith(seg)?(replace?str+this.slice(seg.length):this.slice(seg.length)):this;
	}
}
if(!("distailing" in String.prototype)){
	/**
	* 指定文字列で終わっている場合、削除する。
	* @param {string} seg マッチ文字列
	* @param {boolean} [replace=false] 削除ではなく置換する
	* @param {string} str 置換する文字列
	* @return {string}
	* 
	* @deprecated untail()を使用すること
	*/
	String.prototype.distailing=function(seg,replace,str){
		return this.untail(seg,replace,str);
	}
}
if(!("untail" in String.prototype)){
	/**
	* 指定文字列で始まっている場合、削除する。
	* @param {string} seg マッチ文字列
	* @param {boolean} [replace=false] 削除ではなく置換する
	* @param {string} str 置換する文字列
	* @return {string}
	*/
	String.prototype.untail=function(seg,replace,str){
		return this.endsWith(seg)?(replace?this.slice(0,-seg.length)+str:this.slice(0,-seg.length)):this;
	}
}
if(!("count" in String.prototype)){
	/**
	* サロゲートペアを考慮して文字列の長さを取得する。
	* @return {number}
	*/
	String.prototype.count=function(){
		var count=0;
		var highStart=0xd800;
		var highEnd=0xdbff;
		var lowStart=0xdc00;
		var lowEnd=0xdfff;
		var high=false;
		for(var i=0;i<this.length;i++){
			var code=this.charCodeAt(i);
			if(code.within(highStart,highEnd)){
				count++;
				high=true;
			}
			else if(!code.within(lowStart,lowEnd)||!high){
				count++;
				high=false;
			}
		}
		return count;
	}
}
