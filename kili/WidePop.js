#title="拡張ポップアップメニュー"
#include "ArrayEx.js"
#include "MouseEmulator.js"
/**
* 拡張PopupMenu。機能は以下の通り。
*<ul>
* <li>配列のみでのメニュー定義</li>
* <li>チェックボックス</li>
* <li>ラジオボタン</li>
* <li>チェック、有効･無効の切り換え</li>
* <li>サブメニュー</li>
* <li>選択時にコマンド実行</li>
* </ul>
* @class
* @param {Array.<Array|Object|WidePop.Item>} menu メニューの定義。各要素は{@link WidePop.Item}もしくはその引数をキーとしたディクショナリまたは引数を順に格納した配列
* 
* @example
* var menu=[
* 	["a",1,function(pop,me){Alert(me.tags[0]);},null,["A","a"]],
* 	[],	//区切り線
* 	["b",20],	//チェック&グループ開始
* 	["c",4],	//"b"がチェック済みなのでチェックされない
* 	["d",4],	//次のflagが0なのでここでグループ終了
* 	["e",1,function(pop,me){return this.caption},[
* 		["-a",2,"e-a"],	//チェックボックス
* 		["-b",1,function(pop,me){return pop.getItemById(me.parent).command()+me.caption}],
* 		["-c",1,"unalbled"]
* 	]],
* 	["f",0]	//無効になっている
* ];
* var p=new WidePop(menu);
* p.getItemFromPosition(5,2).enabled=false;
* p.check(1,function(pos,me){return me.caption=="-a"});
* p.create().track(0,true);
* Alert(p.getResult().value)
*/
function WidePop(menu){
	/**
	* ポップアップメニューの実体。
	* @type {PopupMenu}
	*/
	var popup;
	/**
	* メニュー定義。
	* @type {Array.<WidePop.Item>}
	*/
	var definition;
	/**
	* ラジオボタングループ。
	* @type {Array.<Array.<WidePop.Item>>}
	*/
	var radioGroups;
	/**
	* 前回の実行結果。
	* @type {WidePop.Result}
	*/
	var result;
	/**
	* メニューをインデックス化したもの。
	* @type {Array.<number,WidePop.Item>}
	*/
	var items;
	
	/**
	* メニュー定義を作成する。
	* @param {Array.<Array>} menu メニューの定義
	*/
	function define(menu){
		definition=new WidePop.Item(null,null,null,menu,null,[0]).children;
		items=[];
		radioGroups=[];
		indexify(definition);
	}
	/**
	* IDでメニューをインデックス化する。
	* @param {Array.<WidePop.Item>} menu メニュー定義。
	*/
	function indexify(menu){
		var radioCreating=false;
		var radioChecked=false;
		for(var i=0;i<menu.length;i++){
			items[menu[i].id]=menu[i];
			if(menu[i].children==null){
				if(menu[i].type==WidePop.ItemType.RadioButton){
					if(!radioCreating){
						radioCreating=true;
						radioGroups.push([]);
					}
					if(menu[i].checked){
						if(radioChecked){
							menu[i].checked=false;
						}
						else{
							radioChecked=true;
						}
					}
					radioGroups[radioGroups.length-1].push(menu[i]);
				}
				else{
					radioCreating=false;
					radioChecked=false;
				}
			}
			else{
				indexify(menu[i].children);
			}
		}
	}
	/**
	* メニューを作成する。
	* @return {WidePop}
	*/
	this.create=function(){
		popup=createSubItem(definition);
		return this;
	}
	/**
	* サブメニューを作成する。
	* @method
	* @param {Array} menu メニュー定義の配列
	* @return {PopupMenu}
	*/
	function createSubItem(menu){
		var subMenu=CreatePopupMenu();
		for(var i=0;i<menu.length;i++){
			if(!menu[i].enabled){
				subMenu.Add(menu[i].caption,menu[i].id,meMenuGrayed);
			}
			else{
				if(menu[i].children==null){
					var t1=menu[i].type==WidePop.ItemType.Separator?meMenuSeparator:0;
					var t2=menu[i].checked?meMenuChecked:0;
					subMenu.Add(menu[i].caption,menu[i].id,t1|t2);
				}
				else{
					subMenu.AddPopup(menu[i].caption,createSubItem(menu[i].children));
				}
			}
		}
		return subMenu;
	}
	/**
	* ポップアップを表示、コマンドを実行する。
	* @param {number} [pos=0] 0ならキャレット位置に表示、1ならマウスカーソル位置に表示する
	* @param {boolean} mouse {@link MouseEmurator}を使ってポップアップの位置を固定するかどうか
	* @return {WidePop.Result}
	*/
	this.track=function(pos,mouse){
		result=new WidePop.Result();
		if(mouse){
			var emulator=new MouseEmulator();
			var point=emulator.getXY();
		}
		while(result.id=popup.Track(pos)){
			var item=this.getItemById(result.id);
			if(item.type==WidePop.ItemType.RadioButton){
				checkRadioButton(item);
				this.create();
				if(mouse&&pos){
					emulator.setXY(point.x,point.y);
				}
			}
			else if(item.type==WidePop.ItemType.CheckBox){
				item.checked=!item.checked;
				this.create();
				if(mouse&&pos){
					emulator.setXY(point.x,point.y);
				}
			}
			else{
				this.execute(result.id);
				break;
			}
		}
		return result;
	}
	/**
	* ポップアップメニューを取得する。
	* @return {PopupMenu}
	*/
	this.getPopup=function(){
		return popup;
	}
	/**
	* 条件にマッチする項目を取得する。
	* @param {WidePop.Command} condition 条件判定関数
	*/
	this.getItems=function(condition){
		var r=[];
		for(var i=1;i<items.length;i++){
			if(condition(this,items[i])){
				r.push(items[i]);
			}
		}
		return r;
	}
	/**
	* 条件にマッチする項目のIDを取得する。
	* @param {WidePop.Command} condition 条件判定関数
	*/
	this.getIds=function(condition){
		var r=[];
		for(var i=1;i<items.length;i++){
			if(condition(this,items[i])){
				r.push(items[i].id);
			}
		}
		return r;
	}
	/**
	* IDから項目を取得する。
	* @param {number} id ID。
	* @return {WidePop.Item}
	*/
	this.getItemById=function(id){
		return items[id];
	}
	/**
	* メニューの位置から項目を取得する。
	* @param {...number} pos メニューでの位置。0起点で区切りも数える。第1引数が最上位での、第2引数からは順に下位階層での位置を表す。
	* @return {WidePop.Item}
	*/
	this.getItemFromPosition=function(pos){
		var r=definition[arguments[0]];
		for(var i=1;i<arguments.length;i++){
			r=r.children[arguments[i]];
		}
		return r;
	}
	/**
	* 前回の実行結果を返す。
	* @return {WidePop.Result}
	*/
	this.getResult=function(){
		return result;
	}
	/**
	* チェックボックスとなっている項目を取得する。
	* @return {Array.<WidePop.Item>}
	*/
	this.getCheckBox=function(){
		var r=[];
		for(var i=1;i<items.length;i++){
			if(items[i].type==WidePop.ItemType.CheckBox){
				r.push(items[i]);
			}
		}
		return r;
	}
	/**
	* チェックされている項目を取得する。
	* @return {Array.<WidePop.Item>}
	*/
	this.getChecked=function(){
		var r=[];
		for(var i=1;i<items.length;i++){
			if(items[i].checked){
				r.push(items[i]);
			}
		}
		return r;
	}
	/**
	* 項目と同じグループのラジオボタンを取得する。
	* @param {WidePop.Item|number} item メニューアイテムもしくはそのID。
	* @return {Object.<WidePop.Item>}
	*/
	this.getRadioGroup=function(item){
		if(typeof(item)=="number"){
			item=this.getItemById(item)
		}
		for(var i=0;i<radioGroups.length;i++){
			if(radioGroups[i].indexOf(item)>-1){
				return radioGroups[i];
			}
		}
		return null;
	}
	/**
	* 項目のチェックを切り換える。<br />
	* ラジオボタンの場合は常にチェックのみ。
	* @param {number} [check=0]
	* <table>
	* <tr><td>-1</td><td>トグル</td></tr>
	* <tr><td>0</td><td>チェック解除</td></tr>
	* <tr><td>1</td><td>チェック</td></tr>
	* </table>
	* @param {Function} condition 切り換える項目の条件判定関数
	* @return {WidePop}
	*/
	this.check=function(check,condition){
		var r=this.getItems(condition);
		for(var i=0;i<r.length;i++){
			var c=check==0?false:check==1?true:check==-1?!r[i].checked:r[i].checked;
			if(r[i].type==WidePop.ItemType.RadioButton){
				if(c){
					checkRadioButton(r[i]);
				}
				else{
					continue;
				}
			}
			else{
				r[i].checked=c;
			}
		}
		return this;
	}
	/**
	* 項目の有効・無効を切り換える。<br />
	* ラジオボタンはチェックされていない場合のみ有効。
	* @param {number} [enable=0]
	* <table>
	* <tr><td>-1</td><td>トグル</td></tr>
	* <tr><td>0</td><td>無効</td></tr>
	* <tr><td>1</td><td>有効</td></tr>
	* </table>
	* @param {Function} condition 切り換える項目の条件判定関数
	* @return {WidePop}
	*/
	this.enable=function(enable,condition){
		var r=this.getItems(condition);
		for(var i=0;i<r.length;i++){
			var t=enable==0?false:enable==1?true:enable==-1?!r[i].enabled:r[i].enabled;
			if(r[i].type!=WidePop.ItemType.RadioButton||!t){
				r[i].enabled=t;
			}
		}
		return this;
	}
	/**
	* コマンドを実行する。{@link WidePop~result}が更新される。
	* @param {WidePop.Item|number} item ID。
	* @return {Object} trackを参照。
	*/
	this.execute=function(item){
		if(typeof(item)=="number"){
			item=this.getItemById(item)
		}
		result.id=item.id;
		result.value=(item.command instanceof Function?item.command(this,item):item.command)||null;
		return result;
	}
	/**
	* ラジオボタンをチェックする
	* @param {WidePop.Item} item チェックする項目
	*/
	function checkRadioButton(item){
		for(var i=0;i<radioGroups.length;i++){
			if(radioGroups[i].indexOf(item)>-1){
				for(var j=0;j<radioGroups[i].length;j++){
					radioGroups[i][j].checked=false;
				}
				break;
			}
		}
		item.checked=true;
	}
	
	if(menu){
		define(menu);
	}
}
/**
* PopupMenuのアイテム。
* @class
* @param {string} [caption=""] メニューアイテムのテキスト
* @param {number} [flag=8] メニューアイテムの種類や状態。隣り合ったラジオボタンは一つのグループとなる
* <table>
* <tr><td>1</td><td>通常のメニューアイテム</td></tr>
* <tr><td>2</td><td>チェックボックス</td></tr>
* <tr><td>4</td><td>ラジオボタン</td></tr>
* <tr><td>8</td><td>区切り線</td></tr>
* <tr><td>16</td><td>チェックする</td></tr>
* <tr><td>32</td><td>無効</td></tr>
* </table>
* @param {WidePop.Command|*} command WidePop.Commandの場合は実行してその戻り値を返す。それ以外の場合はその値をそのまま返す
* @param {Array.<Array>} children サブメニューの定義
* @param {*} tag 任意のデータ
* @param {Array.<number>} id ID。再帰した関数内で加算するために配列になっている
* @param {number} [parent] 親のID
*/
WidePop.Item=function(caption,flag,command,children,tags,id,parent){
	this.caption=caption||"";
	this.command=command;
	this.enabled=!(flag&32);
	this.checked=!!(flag&16);
	this.type=(flag==null||flag&WidePop.ItemType.Separator)?WidePop.ItemType.Separator:
					flag&WidePop.ItemType.CheckBox?WidePop.ItemType.CheckBox:
					flag&WidePop.ItemType.RadioButton?WidePop.ItemType.RadioButton:
					WidePop.ItemType.Normal;
	this.children;
	this.tags=tags;
	this.id=arguments[5][0];
	arguments[5][0]+=1;
	this.parent=arguments[6]||null;
	if(children instanceof Array){
		this.children=[];
		for(var i=0;i<children.length;i++){
			if(children[i] instanceof Array){
				this.children.push(new WidePop.Item(children[i][0],children[i][1],children[i][2],children[i][3],children[i][4],arguments[5],this.id));
			}
			else if(children[i] instanceof WidePop.Item){
				this.children.push(children[i]);
			}
			else if(typeof(children[i])=="object"){
				this.children.push(new WidePop.Item(children[i].caption,children[i].flag,children[i].command,children[i].children,children[i].tags,arguments[5],this.id));
			}
		}
	}
}
/**
* メニュー項目のタイプ。
* @enum {number}
*/
WidePop.ItemType={
	Normal:1,
	CheckBox:2,
	RadioButton:4,
	Separator:8
}
/**
* 実行結果。
*/
WidePop.Result=function(){
	/**
	* 選択された項目のID。
	* @type {number}
	*/
	this.id;
	/**
	* コマンドの実行結果。
	* @type {*}
	*/
	this.value;
}
/**
* 条件判定に使用されたり、項目選択時に実行されるコマンド。
* @typedef {function(WidePop,WidePop.Item):*} 第1引数はインスタンスそのもの、第2引数は選択された項目
*/
WidePop.Command;
/**
* キャプションとコマンドのみからなる省略形のメニュー定義配列を標準形に変換する。
* @param {Array.<Array>} arr メニューの定義
*/
WidePop.normalize=function(arr){
	for(var i=0;i<arr.length;i++){
		arr[i]=[arr[i][0]||null,0,arr[i][1]||null,null];
	}
	return arr;
}