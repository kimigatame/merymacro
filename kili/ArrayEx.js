#title="Arrayオブジェクト拡張"
#include "json2.js"
////////////////////////////////////////////////////////////////////////////////
//■概要:
//Arrayオブジェクト拡張。
////////////////////////////////////////////////////////////////////////////////

if(!("indexOf" in Array.prototype)){
	/**
	* 配列から検索する。
	* @param {Object} obj 検索要素
	* @param {number} [from=0] 検索開始インデックス
	* @return {number} 配列インデックス。見つからなければ-1
	*/
	Array.prototype.indexOf=function(obj,from){
		var t=JSON.stringify(obj);
		for(var i=from||0;i<this.length;i++){
			if(JSON.stringify(this[i])==t){
				return i;
			}
		}
		return -1;
	};
}
if(!("mine" in Array.prototype)){
	/**
	* 指定条件にマッチする要素を集める。
	* @param {function(*):boolean} func 条件を判定するコールバック
	* @param {boolan} [returnElement=false] trueなら要素自体を返す、falseならインデックスを返す
	* @return {Array.<*>}
	* 
	* @deprecated select()、selectIndex()を使用すること
	* 
	* @example
	* //return [0,2,4]
	* [1,2,3,4,5].mine(function(e){return e%2;});
	* return [1,3,5]
	* [1,2,3,4,5].mine(function(e){return e%2;},true);
	*/
	Array.prototype.mine=function(func,returnElement){
		if(returnElement){
			return this.select(func);
		}
		else{
			return this.selectIndex(func);
		}
	};
}

/**
* 選別用関数。
* @callback Array.Selector
* @param {string|number} index インデックス
* @param {Object} value 要素そのもの
*/

if(!("select" in Array.prototype)){
	/**
	* 指定条件にマッチする要素を集める。
	* @param {...Array.Selector} selectors 選別用関数。複数指定した場合はORマッチを行う
	* @return {Array.<*>}
	* 
	* @example
	* return [1,3,5]
	* [1,2,3,4,5].select(function(index,value){return value%2;});
	*/
	Array.prototype.select=function(selectors){
		return this.pickup.apply(this,this.selectIndex(selectors));
	};
}
if(!("selectIndex" in Array.prototype)){
	/**
	* 指定条件にマッチする要素のインデックスを集める。
	* @param {...Array.Selector} selectors 選別用関数。複数指定した場合はORマッチを行う
	* @return {Array.<*>}
	* 
	* @example
	* //return [0,2,4]
	* [1,2,3,4,5].selectIndex(function(index,value){return value%2;});
	*/
	Array.prototype.selectIndex=function(selectors){
		var result=[];
		for(var i=0;i<this.length;i++){
			for(var j=0;j<arguments.length;j++){
				if(arguments[j](i,this[i])){
					result.push(i);
					break;
				}
			}
		}
		return result;
	};
}
if(!("undup" in Array.prototype)){
	/**
	* 配列から重複を削除する。
	* @return {Array.<*>}
	* 
	* @deprecated unique()を使用すること
	*/
	Array.prototype.undup=function(){
		return this.unique();
	};
}
if(!("unique" in Array.prototype)){
	/**
	* 配列から重複を削除する。
	* @return {Array.<*>}
	*/
	Array.prototype.unique=function(){
		var obj={};
		for(var i=0;i<this.length;){
			var s=JSON.stringify(this[i]);
			if(s in obj){
				this.splice(i,1);
			}
			else{
				obj[s]=1;
				i++;
			}
		}
		return this;
	};
}
if(!("clone" in Array.prototype)){
	/**
	* 配列を値渡しする。
	* @return {Array.<*>}
	*/
	Array.prototype.clone=function(){
		return JSON.parse(JSON.stringify(this));
	};
}
if(!("del" in Array.prototype)){
	/**
	* 一致した要素を削除する。
	* @param {...*} args 削除する要素
	* @return {Array.<*>}
	* 
	* @deprecated remove()を使用すること
	*/
	Array.prototype.del=function(args){
		return this.remove.apply(this,arguments);
	};
}
if(!("remove" in Array.prototype)){
	/**
	* 一致した要素を削除する。
	* @param {...*} args 削除する要素
	* @return {Array.<*>}
	*/
	Array.prototype.remove=function(args){
		var t;
		for(var i=0;i<arguments.length;){
			t=this.indexOf(arguments[i]);
			(t>-1)?this.splice(t,1):i++;
		}
		return this;
	};
}
if(!("last" in Array.prototype)){
	/**
	* 配列の最後の要素を返す。
	* @return {*}
	*/
	Array.prototype.last=function(){
		return this[this.length-1];
	};
}
if(!("compact" in Array.prototype)){
	/**
	* 空文字列、null、undefinedを削除する。
	* @return {Array.<*>}
	*/
	Array.prototype.compact=function(){
		return this.remove("",null,undefined);
	};
}
if(!("siblings" in Array.prototype)){
	/**
	* 各要素から指定したプロパティの値を取得する。
	* @param {string} name プロパティ名
	* @return {Array.<Object>}
	* 
	* @example
	* //return [1,3,5]
	* [{a:1,b:2},{a:3,c:4},{a:5,d:6}].siblings("a");
	*/
	Array.prototype.siblings=function(name){
		var result=[];
		for(var i=0;i<this.length;i++){
			if(this[i] instanceof Object&&name in this[i]){
				result.push(this[i][name]);
			}
		}
		return result;
	};
}
if(!("pickup" in Array.prototype)){
	/**
	* 指定したインデックスの要素を取り出した配列を返す。
	* @param {...number} indices 取り出す要素のインデックス
	* @return {Array.<*>}
	* 
	* @example
	* //[3,6,2]
	* [1,2,3,4,5,6].pickup(2,5,1);
	*/
	Array.prototype.pickup=function(indices){
		var arr=[];
		var len=this.length-1;
		for(var i=0;i<arguments.length;i++){
			if(arguments[i]<=len&&arguments[i]>=0){
				arr.push(this[arguments[i]]);
			}
		};
		return arr;
	};
}
if(!("insert" in Array.prototype)){
	/**
	* 要素を挿入する。
	* @param {number} pos 挿入位置
	* @param {*} item 挿入要素
	* @return {Array}
	*/
	Array.prototype.insert=function(pos,item){
		this.splice(pos,0,item);
		return this;
	};
}
