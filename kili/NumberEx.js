#title="Numberオブジェクト拡張"
////////////////////////////////////////////////////////////////////////////////
//■概要:
//Numberオブジェクト拡張。
////////////////////////////////////////////////////////////////////////////////

if(!("padding" in Number.prototype)){
	/** 
	* 桁揃えする。<br />
	* 文字列としての長さがdigitより長い場合、切り捨ては行わない。
	* @param {number} [digit=4] 全体の桁数
	* @param {string} [padding=0] 埋め文字
	* @param {number} [radix=10] 文字列に変換する際の基数
	* @return {string}
	*/
	Number.prototype.padding=function(digit,pad,radix){
		digit=digit||4;
		radix=radix||10;
		var seg=this.toString(radix);
		return (seg.length<=digit?new Array(digit-seg.length+1).join(pad||"0"):"")+seg;
	}
}
if(!("inRange" in Number.prototype)){
	/**
	* n以上m以下であるか判定する。
	* @param {!number} r1 最大値または最小値
	* @param {!number} r2 最大値または最小値
	* @return {boolean}
	* 
	* @deprecated within()を使用すること
	*/
	Number.prototype.inRange=function(r1,r2){
		return this.within(r1,r2)
	}
}
if(!("within" in Number.prototype)){
	/**
	* n以上m以下であるか判定する。
	* @param {!number} r1 最大値または最小値
	* @param {!number} r2 最大値または最小値
	* @return {boolean}
	*/
	Number.prototype.within=function(r1,r2){
		if(r2>r1){
			return r1<=this&&this<=r2;
		}
		else{
			return r2<=this&&this<=r1;
		}
	}
}
if(!("toHexString" in Number.prototype)){
	/**
	* 十六進数表記で返す。
	* @return {string}
	*/
	Number.prototype.toHexString=function(){
		return "0x"+this.toString(16);
	}
}
if(!("constrain" in Number.prototype)){
	/**
	* 数値を範囲内に収める。
	* @param {!number} r1 最大値または最小値
	* @param {!number} r2 最大値または最小値
	* @return {number}
	* 
	* @deprecated clamp()を使用すること
	*/
	Number.prototype.constrain=function(r1,r2){
		return this.clamp(r1,r2);
	}
}
if(!("clamp" in Number.prototype)){
	/**
	* 数値を範囲内に収める。
	* @param {!number} r1 最大値または最小値
	* @param {!number} r2 最大値または最小値
	* @return {number}
	*/
	Number.prototype.clamp=function(r1,r2){
		if(r2>=r1){
			var t=this>=r1?this:r1;
			return r2>=t?t:r2;
		}
		else{
			var t=this>=r2?this:r2;
			return r1>=t?t:r1;
		}
	}
}
if(!("sign" in Number.prototype)){
	/**
	* 符号を取得する。
	* @return {number} 負の値なら-1、0なら0、正の値なら1
	*/
	Number.prototype.sign=function(){
		return this==0?0:this<0?-1:1;
	}
}
