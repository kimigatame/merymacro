#title="KILIライブラリベース"
////////////////////////////////////////////////////////////////////////////////
//■概要:
//よく使うかもしれない処理いろいろ。
////////////////////////////////////////////////////////////////////////////////

/**
* 論理座標で指定する。
* @global
* @constant {number}
*/
var meGetLineLogical=0;
/**
* キャレット位置で指定する。
* @global
* @constant {number}
*/
var mePosCaret=0;

/**
* @namespace
*/
var KILI;
if(KILI==null){
 	KILI={};
	/**
	* 座標を表す。
	* @class
	* @param {number} x X座標
	* @param {number} y Y座標
	*/
	KILI.Point=function(x,y){
		/**
		* X座標。
		* @type {number}
		*/
		this.x=x<0?-1:x;
		/**
		* Y座標。
		* @type {number}
		*/
		this.y=y<0?-1:y;
		/**
		* 配列に変換する。
		* @type {Array.<number>}
		*/
		this.toArray=function(){
			return [this.x,this.y];
		}
	}
	/**
	* 選択範囲を表す。
	* @class
	*/
	KILI.Range=function(){
		/**
		* 文頭側の境界。
		* @type {number}
		*/
		this.top=0;
		/**
		* 文末側の境界。
		* @type {number}
		*/
		this.bottom=0;
		/**
		* 選択開始位置。
		* @type {number}
		*/
		this.active=0;
		/**
		* 選択終了位置。
		* @type {number}
		*/
		this.anchor=0;
		/**
		* 選択方向。
		* @type {KILI.Direction}
		*/
		this.direction=KILI.Direction.none;
		/**
		* 座標系。
		* @type {KILI.Coordinate}
		*/
		this.type=KILI.Coordinate.pos;
	}
	/**
	* 選択方向。
	* @class
	*/
	KILI.Direction={
		/**
		* 文頭方向。
		*/
		backward:-1,
		/**
		* 選択なし。
		*/
		none:0,
		/**
		* 文末方向。
		*/
		forward:1
	};
	/**
	* 座標系。
	* @class
	*/
	KILI.Coordinate={
		/**
		* 文頭からの位置。
		*/
		pos:0,
		/**
		* 論理行。
		*/
		logical:1,
		/**
		* 表示行。
		*/
		view:2
	};
	/**
	* 行モードに対応する座標系を取得する。
	* @static
	* @param {KILI.LineMode} lineMode 行モード
	*/
	KILI.Coordinate.get=function(lineMode){
		return lineMode==KILI.LineMode.view?KILI.Coordinate.view:KILI.Coordinate.logical;
	}
	/**
	* 単位として扱う行。
	* @class
	*/
	KILI.LineMode={
		/**
		* 論理行。
		*/
		logical:false,
		/**
		* 表示行。
		*/
		view:true
	}
	/**
	* Mery組み込みの変数に変換する。
	* @static
	* @param {KILI.LineMode} lineMode 行モード
	*/
	KILI.LineMode.convert=function(lineMode){
		return lineMode==KILI.LineMode.view?mePosView:mePosLogical;
	}
}
var O;
if(O==null){
	/**
	* OutputBar.Writeへのショートカット。
	* @param {?object} data 出力データ
	* @param {boolean} [nobreak=false] 改行しない
	* @param {boolean} [clear=false] 出力前にウィンドウをクリアする
	* @param {boolean} [focus=false] 出力ウィンドウにフォーカスを移す
	*/
	O=function(data,nobreak,clear,focus){
		OutputBar.Visible=true;
		if(clear){
			OutputBar.Clear();
		}
		switch(data){
			case null:
				data="*null";
				break;
			case undefined:
				data="*undefined";
				break;
			default:
				try{
					data=data.toString();
				}
				catch(e){
					data=typeof(data)=="object"?"[object Object]":data;
				}
		}
		OutputBar.Write(data+(nobreak?"":"\n"));
		if(focus){
			OutPutBar.SetFocus();
		}
	}
}

var GEO;
if(GEO==null){
	/**
	* Editorを取得する。
	* @param {number|Editor} eIndex エディタのインデックス。不正な値の場合はアクティブ
	* @return {Editor}
	*/
	GEO=function(eIndex){
		if(eIndex&&typeof(eIndex)=="object"&&"FindInFiles" in eIndex&&"ReplaceInFiles" in eIndex){
			return eIndex;
		}
		eIndex=parseInt(eIndex);
		return isNaN(eIndex)||eIndex<0||eIndex>=Editors.Count?Editor:Editors.Item(eIndex);
	}
}

var GDO;
if(GDO==null){
	/**
	* Documentを取得する。
	* @param {number|Document} dIndex 文書のインデックス。不正な値の場合はアクティブ
	* @param {number} eIndex エディタのインデックス。不正な値の場合はアクティブ
	* @return {Document}
	*/
	GDO=function(dIndex,eIndex){
		if(dIndex&&typeof(dIndex)=="object"&&"CopyFullName" in dIndex&&"HighlightFind" in dIndex){
			return dIndex;
		}
		var e=GEO(eIndex);
		dIndex=parseInt(dIndex);
		return isNaN(dIndex)||dIndex<0||dIndex>=e.Documents.Count?e.ActiveDocument:e.Documents.Item(dIndex);
	}
}

var GSO;
if(GSO==null){
	/**
	* Selectionを取得する。
	* @param {Document|number} $d 対象のDocument。もしくは文書のインデックス。不正な値の場合はアクティブ
	* @param {number} eIndex $dでnumberを指定した場合、エディタのインデックス
	* @return {Selection}
	*/
	GSO=function($d,eIndex){
		return GDO($d,eIndex).Selection;
	}
}

var Seiko;
if(Seiko==null){
	/**
	* 時間を計測する
	* @class
	*/
	Seiko=function(){
		/**
		* 計測開始時。
		* @type {number}
		*/
		this.begin=0;
		/**
		* 計測終了時。
		* @type {number}
		*/
		this.end=0;
		/**
		* 経過時間。
		* @type {number}
		*/
		this.elapsed=0;
		/**
		* 計測を開始する。
		*/
		this.start=function(){
			this.elapsed=0;
			this.begin=new Date().getTime();
		}
		/**
		* 計測を終了する。
		* @return {Seiko}
		*/
		this.goal=function(){
			this.end=new Date().getTime();
			this.elapsed=this.end-this.begin;
			return this;
		}
	}
	/**
	* 関数を指定回数実行して時間計測する
	* @static
	* @param {Function} func 計測する関数
	* @param {number} count 実行回数
	* @return {number} 経過時間
	*/
	Seiko.run=function(func,count){
		var s=new Seiko(),time=0;
		for(var i=0;i<count;i++){
			s.start();
			func();
			time+=s.goal().elapsed;
		}
		return time;
	};
}

if(KILI.selex==null){
	/**
	* Selection拡張。選択範囲・キャレット位置に関するもの。
	* @class
	*/
	KILI.selex={};
	/**
	* 複数の行を選択する。
	* @param {!number} from 開始行
	* @param {!number} to 終了行
	* @param {boolean} [excludeBreak=false] trueならば文末側の改行を除外する、falseならば除外しない
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.selex.selectLine=function(from,to,excludeBreak,lineMode,$d){
		var t=to-from;
		KILI.selex.selectLines(from,t<0?t-1:t+1,excludeBreak,lineMode,$d);
	}
	/**
	* 複数の行を選択する。
	* @param {!number} from 開始行
	* @param {number} num 選択行数。0ならば選択しない、負ならば文頭方向へ選択する
	* @param {boolean} [excludeBreak=false] trueならば文末側の改行を除外する、falseならば除外しない
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.selex.selectLines=function(from,count,excludeBreak,lineMode,$d){
		if(!count){
			return;
		}
		var target=lineMode?meGetLineView:meGetLineLogical;
		var $s=($d=GDO($d)).Selection;
		var lines=$d.GetLines(target);
		var active=new KILI.Point(1,1);
		var anchor=new KILI.Point(1,Math.min(Math.max(from,1),lines));
		if(count>0){
			active.y=anchor.y+count;
			if(excludeBreak){
				active.y-=1;
				active.x=$d.GetLine(active.y,target).length+1;
			}
		}
		else{
			active.y=Math.max(anchor.y+count+1,1);
			if(excludeBreak){
				anchor.x=$d.GetLine(anchor.y,target).length+1;
			}
			else{
				anchor.y+=1;
			}
		}
		KILI.selex.setRange(anchor,active,lineMode,$d);
	}
	/**
	* 選択されていなければ選択する。
	* @param {number} [range=0] 選択範囲。0ならば全体、1ならば論理行、2ならば表示行
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.selex.selectIfEmpty=function(range,$d){
		var $s=($d=GDO($d)).Selection;
		if($s.IsEmpty){
			Redraw=false;
			if(!range){
				$s.SelectAll();
			}
			else if(range==1){
				$s.SelectLine();
			}
			else if(range==2){
				$s.StartOfLine();
				$s.EndOfLine(true);
				if($d.Text.charAt($s.GetActivePos())=="\n"){
					$s.CharRight(true);//SelectLineに合わせて\nも選択
				}
			}
			Redraw=true;
		}
	}
	/**
	* 論理行座標を表示行座標に変換する。
	* @param {!KILI.Point|number} x 論理行の座標または論理行桁
	* @param {number} [y] @xがnumberのとき、論理行
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.logicalToView=function(x,y,$d){
		$d=GDO($d);
		var views=0;
		var lineCount=$d.GetLines(meGetLineView);
		if(x instanceof KILI.Point){
			y=x.y;
			x=x.x;
		}
		x=Math.max(x,1);
		y=Math.min($d.GetLines(meGetLineLogical),Math.max(y,1));
		var i=1;
		var logical=$d.GetLine(i,meGetLineLogical).length+1;//行のインデックスが1始まりの為
		for(var j=1;j<=lineCount;j++){
			var v=$d.GetLine(j,meGetLineView).length;
			var logicalX=Math.min(x,logical);
			views+=v;
			if(i==y&&views+1>=logicalX){
				return new KILI.Point(v-(views-logicalX),j);
			}
			if(logical==views+1){
				logical=$d.GetLine(++i,meGetLineLogical).length+1;
				views=0;
			}
		}
		return new KILI.Point(-1,-1);//ここへは辿り着かないはず
	}
	/**
	* 論理行座標を文頭からの位置に変換する。
	* @param {!KILI.Point|number} x 論理行の座標または論理行桁
	* @param {number} [y] @xがnumberのとき、論理行
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.logicalToPos=function(x,y,$d){
		var pat=/^/gm;
		var text=GDO($d).Text;
		var c=0;
		if(x instanceof KILI.Point){
			y=x.y;
			x=x.x;
		}
		while(pat.test(text)){
			if(++c==y){
				break;
			}
		}
		return RegExp.index+x-1;
	}
	/**
	* 文頭からの位置を論理行座標に変換する。
	* @param {!number} pos 文頭からの位置
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.posToLogical=function(pos,$d){
		var pat=/^/gm;
		var text=GDO($d).Text;
		var c=0;
		var prev=0;
		while(pat.test(text)){
			if(RegExp.index<=pos){
				c++;
				prev=RegExp.index;
				continue;
			}
			break;
		}
		return new KILI.Point(pos-prev+1,c);
	}
	/**
	* 文頭からの位置を表示行座標に変換する
	* @param {!number} pos 文頭からの位置
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.posToView=function(pos,$d){
		$d=GDO($d)
		pos=Math.min($d.Text.length,Math.max(pos,0));
		var length=$d.Text.slice(0,pos).split("\n").length-1;//改行分
		var lineCount=$d.GetLines(meGetLineView);
		for(var i=1;i<=lineCount;i++){
			var v=$d.GetLine(i,meGetLineView).length;
			length+=v;
			if(length>=pos){
				return new KILI.Point(v-(length-pos)+1,i);
			}
		}
		return new KILI.Point(-1,-1);//ここへは辿り着かないはず
	}
	/**
	* 表示行座標を論理行座標に変換する。
	* @param {!KILI.Point|number} x 表示行の座標または表示行桁
	* @param {number} [y] @xがnumberのとき、表示行
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.viewToLogical=function(x,y,$d){
		$d=GDO($d);
		var views=0;
		if(x instanceof KILI.Point){
			y=x.y;
			x=x.x;
		}
		x=Math.max(x,1);
		y=Math.min($d.GetLines(meGetLineView),Math.max(y,1));
		var i=1;
		var logical=$d.GetLine(i,meGetLineLogical).length;
		for(var j=1;j<=y;j++){
			var v=$d.GetLine(j,meGetLineView).length;
			if(j==y){
				return new KILI.Point(views+Math.min(x,v+1),i);
			}
			views+=v;
			if(logical==views){
				logical=$d.GetLine(++i,meGetLineLogical).length;
				views=0;
			}
		}
		return new KILI.Point(-1,-1);//ここへは辿り着かないはず
	}
	/**
	* 表示行・桁を文書先頭からの位置に変換
	* @param {!KILI.Point|number} x 表示行の座標または表示行桁
	* @param {number} [y] @xがnumberのとき、表示行
	* @param {?Document} $d 対象となるDocument
	* @return {number}
	*/
	KILI.selex.viewToPos=function(x,y,$d){
		$d=GDO($d);
		var views=0;
		var total=0;
		if(x instanceof KILI.Point){
			y=x.y;
			x=x.x;
		}
		x=Math.max(x,1);
		y=Math.min($d.GetLines(meGetLineView),Math.max(y,1));
		var i=1;
		var logical=$d.GetLine(i,meGetLineLogical).length;
		for(var j=1;j<=y;j++){
			var v=$d.GetLine(j,meGetLineView).length;
			if(j==y){
				return total+Math.min(x-1,v);
			}
			total+=v;
			views+=v;
			if(logical==views){
				logical=$d.GetLine(++i,meGetLineLogical).length;
				total+=1;
				views=0;
			}
		}
		return -1;//ここへは辿り着かないはず
	}
	/**
	* 論理行頭かどうか判定する。
	* @param {KILI.Point|number|undefined} [pos=undefined] 表示行座標または文頭からの位置。undefinedのときは現在位置
	* @param {?Document} $d 対象となるDocument
	* @return {boolean}
	*/
	KILI.selex.atLogicalHead=function(pos,$d){
		$d=GDO($d);
		if(pos==undefined){
			return $d.Selection.GetActivePointX(mePosLogical)==1;
		}
		if(typeof(pos)=="number"){
			return pos==0||$d.Text.charAt(pos-1)=="\n";
		}
		return KILI.selex.viewToLogical(pos,null,$d).x==1;
	}
	/**
	* 論理行末かどうか判定する。
	* @param {KILI.Point|number|undefined} [pos=undefined] 表示行座標または文頭からの位置。undefinedのときは現在位置
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.view] posの{@link KILI.Point}の行モード
	* @return {boolean}
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.selex.atLogicalEnd=function(pos,lineMode,$d){
		$d=GDO($d);
		if(lineMode==undefined){
			lineMode=KILI.LineMode.view;
		}
		if(pos==undefined){
			pos=$d.Selection.GetActivePos();
		}
		else if(typeof(pos)!="number"){
			pos=lineMode==KILI.LineMode.view?KILI.selex.viewToPos(pos,null,$d):KILI.selex.logicalToPos(pos,null,$d);
		}
		return $d.Text.charAt(pos)=="\n"||pos==$d.Text.length;
	}
	/**
	* 表示行頭かどうか判定する。
	* @param {KILI.Point|number|undefined} [pos=undefined] 論理行座標または文頭からの位置。undefinedのときは現在位置
	* @param {?Document} $d 対象となるDocument
	* @return {boolean}
	*/
	KILI.selex.atViewHead=function(pos,$d){
		$d=GDO($d);
		if(pos==undefined){
			return $d.Selection.GetActivePointX(mePosView)==1;
		}
		if(typeof(pos)=="number"){
			return KILI.selex.posToView(pos,$d).x==1;
		}
		return KILI.selex.logicalToView(pos,null,$d).x==1;
	}
	/**
	* 表示行末かどうか判定する。
	* @param {KILI.Point|number|undefined} [pos=undefined] 論理行座標または文頭からの位置。undefinedのときは現在位置
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] posの{@link KILI.Point}の行モード
	* @param {?Document} $d 対象となるDocument
	* @return {boolean}
	*/
	KILI.selex.atViewEnd=function(pos,lineMode,$d){
		var $s=($d=GDO($d)).Selection;
		if(pos==undefined){
			pos=KILI.selex.getActivePoint(true,$d);
		}
		else if(typeof(pos)=="number"){
			pos=KILI.selex.posToView(pos,$d);
		}
		else if(lineMode==KILI.LineMode.logical){
			pos=KILI.selex.logicalToView(pos,null,$d);
		}
		return $d.GetLine(pos.y,meGetLineView).length+1==pos.x;
	}
	/**
	* 文末かどうか判定する。
	* @param {KILI.Point|number|undefined} [pos=undefined] 論理行座標または文頭からの位置。undefinedのときは現在位置
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] posの{@link KILI.Point}の行モード
	* @param {?Document} $d 対象となるDocument
	* @return {boolean}
	*/
	KILI.selex.atEOF=function(pos,lineMode,$d){
		var $s=($d=GDO($d)).Selection;
		if(pos==undefined){
			pos=$s.GetActivePos();
		}
		else if(typeof(pos)!="number"){
			pos=lineMode==KILI.LineMode.view?KILI.selex.viewToPos(pos,null,$d):KILI.selex.logicalToPos(pos,null,$d);
		}
		return pos==$d.Text.length;
	}
	/**
	* 行内選択かどうか判定する。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {boolean}
	*/
	KILI.selex.inline=function(lineMode,$d){
		var $s=GDO($d).Selection;
		var type=KILI.LineMode.convert(lineMode);
		return $s.GetActivePointY(type)==$s.GetAnchorPointY(type);
	}
	/**
	* 複数行に渡る選択かどうか判定する。
	* KILI.selex.wholeLinesでは一行となってもtrueになる場合がある。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {boolean}
	*/
	KILI.selex.multiline=function(lineMode,$d){
		var $s=GDO($d).Selection;
		var type=KILI.LineMode.convert(lineMode);
		return $s.GetActivePointY(type)!=$s.GetAnchorPointY(type);
	}
	/**
	* 行の全体を選択しているかどうか判定する。<br />
	* 行番号クリック時に合わせているので、最後の改行を含まないと全体とは見なさない。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {number} 選択行数。全体を選択していない場合は0
	*/
	KILI.selex.wholeLines=function(lineMode,$d){
		$d=GDO($d);
		var lines=0;
		var range=KILI.selex.getRange(KILI.Coordinate.get(lineMode),$d);
		if(range.top.x==1){
			var l=range.bottom.y-range.top.y;
			if(l>0&&range.bottom.x==1){
				lines=l;
			}
			else if(lineMode==KILI.LineMode.view?KILI.selex.atViewEnd(range.bottom,true,$d):KILI.selex.atEOF(range.bottom,false,$d)){
				lines=l+1;
			}
		}
		return lines;
	}
	/**
	* テキスト選択方向を取得する。
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Direction} 選択方向
	*/
	KILI.selex.getDirection=function($d){
		var $s=GSO($d);
		return $s.IsEmpty?KILI.Direction.none:$s.GetAnchorPos()>$s.GetActivePos()?KILI.Direction.backward:KILI.Direction.forward;
	}
	/**
	* 選択範囲を取得する。
	* @param {KILI.Coordinate} [coordinate=KILI.Coordinate.pos] 座標系
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Range}
	*/
	KILI.selex.getRange=function(coordinate,$d){
		var $s=GSO($d);
		var anchor,active;
		var range=new KILI.Range();
		if(!coordinate){
			range.anchor=$s.GetAnchorPos();
			range.active=$s.GetActivePos();
			range.type=0;
			range.top=Math.min(range.active,range.anchor);
			range.bottom=Math.max(range.active,range.anchor);
			range.direction=range.anchor>range.active?-1:range.anchor<range.active?1:0;
		}
		else{
			if(coordinate!=KILI.Coordinate.logical){
				range.type=KILI.Coordinate.view;
			}
			range.anchor=KILI.selex.getAnchorPoint(coordinate-1,$d);
			range.active=KILI.selex.getActivePoint(coordinate-1,$d);
			if(
				range.anchor.y>range.active.y||
				range.anchor.y==range.active.y&&range.anchor.x>range.active.x
			){
				range.top=range.active;
				range.bottom=range.anchor;
				range.direction=KILI.Direction.backward;
			}
			else if(range.anchor.x==range.active.x&&range.anchor.y==range.anchor.y){
				range.top=range.anchor;
				range.bottom=range.active;
				range.direction=KILI.Direction.none;
			}
			else{
				range.top=range.anchor;
				range.bottom=range.active;
				range.direction=KILI.Direction.forward;
			}
		}
		return range;
	}
	/**
	* 範囲選択する。
	* @param {number|KILI.Point|KILI.Range} anchor 開始位置。numberならば文頭からの位置、{@link KILI.Point}ならば論理行座標
	* 	{@link KILI.Range}ならばactiveとlineModeは不要
	* @param {number|KILI.Point} active 終了位置。numberならば文頭からの位置、{@link KILI.Point}ならば論理行座標
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.selex.setRange=function(anchor,active,lineMode,$d){
		var $s=GSO($d);
		var activeX,activeY,anchorX,anchorY;
		if(anchor instanceof KILI.Range){
			lineMode=anchor.type==KILI.Coordinate.view?KILI.LineMode.view:KILI.LineMode.logical;
			active=anchor.active;
			anchor=anchor.anchor;
		}
		var posType=KILI.LineMode.convert(lineMode);
		Redraw=false;
		anchor instanceof KILI.Point?$s.SetAnchorPoint(posType,anchor.x,anchor.y):$s.SetAnchorPos(anchor);
		active instanceof KILI.Point?$s.SetActivePoint(posType,active.x,active.y,true):$s.SetActivePos(active,true);
		Redraw=true;
	}
	/**
	* 選択範囲を反転する。
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.selex.reverse=function($d){
		var $s=GSO($d);
		if(!$s.IsEmpty){
			KILI.selex.setRange($s.GetActivePos(),$s.GetAnchorPos(),$d);
		}
	}
	/**
	* 選択範囲の文末側の位置を取得する。
	* @param {?Document} $d 対象となるDocument
	* @return {number}
	*/
	KILI.selex.getBottomPos=function($d){
		var $s=GSO($d);
		return Math.max($s.GetAnchorPos(),$s.GetActivePos());
	}
	/**
	* 選択範囲の文頭側の位置を取得する。
	* @param {?Document} $d 対象となるDocument
	* @return {number}
	*/
	KILI.selex.getTopPos=function($d){
		var $s=GSO($d);
		return Math.min($s.GetAnchorPos(),$s.GetActivePos());
	}
	/**
	* カーソル位置を取得する。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.getAnchorPoint=function(lineMode,$d){
		var $s=GSO($d);
		lineMode=KILI.LineMode.convert(lineMode);
		return new KILI.Point($s.GetAnchorPointX(lineMode),$s.GetAnchorPointY(lineMode));
	}
	/**
	* 選択範囲の開始位置を取得する。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.getActivePoint=function(lineMode,$d){
		var $s=GSO($d);
		lineMode=KILI.LineMode.convert(lineMode);
		return new KILI.Point($s.GetActivePointX(lineMode),$s.GetActivePointY(lineMode));
	}
	/**
	* 選択範囲の文末側の位置を取得する。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.getBottomPoint=function(lineMode,$d){
		var $s=GSO($d);
		lineMode=KILI.LineMode.convert(lineMode);
		return new KILI.Point($s.GetBottomPointX(lineMode),$s.GetBottomPointY(lineMode));
	}
	/**
	* 選択範囲の文頭側の位置を取得する。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {KILI.Point}
	*/
	KILI.selex.getTopPoint=function(lineMode,$d){
		var $s=GSO($d);
		lineMode=KILI.LineMode.convert(lineMode);
		return new KILI.Point($s.GetTopPointX(lineMode),$s.GetTopPointY(lineMode));
	}
	/**
	* 文字列を文書全体から探して位置を取得する。
	* @param {?string|RegExp} seg 検索文字列。指定しなければ選択部分。
	* 	"``"で囲まれた文字列の場合、String.onigFindで検索する
	* @param {boolean} [ignoreCase=false] {@link String.onigFind}で大文字と小文字を区別しない
	* @param {?Document} $d 対象となるDocument
	* @return {Array}
	*/
	KILI.selex.search=function(seg,ignoreCase,$d){
		var $s=($d=GDO($d)).Selection;
		seg=seg instanceof RegExp?new RegExp(seg.source,"g"):seg||$s.Text;
		var result=[];
		result.match=[];
		var pos=0;
		var text=$d.Text;
		if(seg!=""){
			if(typeof(seg)=="string"){
				if(seg.enclosed("``")){//鬼車
					seg=seg.slice(2,-2);
					var t=text.onigFind(seg,ignoreCase,null,$d);
					for(var i=0;i<t.length;i++){
						result.push(t[i][0]);
						result.match.push(t[i][1]);
					}
				}
				else{//文字列
					while((pos=text.indexOf(seg,pos))>-1){
						result.push(pos);
						pos+=seg.length;
					}
				}
			}
			else{//正規表現
				while(seg.test(text)){
					result.push(RegExp.index);
					result.match.push(RegExp.lastMatch);
				}
			}
		}
		return result;
	}
	/**
	* 全ての論理行頭の位置を取得する。
	* @param {?Document} $d 対象となるDocument
	* @return {Array}
	*/
	KILI.selex.getLogicalHeadPos=function($d){
		var pat=/^/gm;
		var result=[];
		var text=GDO($d).Text;
		while(pat.test(text)){
			result.push(RegExp.index);
		}
		return result;
	}
	/**
	* 全ての論理行末の位置を取得する。
	* @param {?Document} $d 対象となるDocument
	* @return {Array}
	*/
	KILI.selex.getLogicalEndPos=function($d){
		var pat=/\n/gm;
		var result=[];
		var text=GDO($d).Text;
		while(pat.test(text)){
			result.push(RegExp.index);
		}
		result.push(text.length);
		return result;
	}
	/**
	* 全ての折り返し位置を取得する。
	* @param {KILI.LineMode} [lineMode=false] 行モード。表示行は論理行とみなす
	* @param {?Document} $d 対象となるDocument
	* @return {Array} lineModeがtrueならば各要素は{@link KILI.Point}
	*/
	KILI.selex.getWrapPos=function(lineMode,$d){
		$d=GDO($d);
		var viewCount=$d.GetLines(meGetLineView);
		var wrapCount=viewCount-$d.GetLines(meGetLineLogical);
		var logicals=$d.Text.split("\n");
		var total=0;
		var index=[];
		var found=0;
		var unwrapped="";
		for(var i=1,j=0;i<=viewCount;i++){
			var line=$d.GetLine(i);
			unwrapped+=line;
			total+=line.length;
			if(unwrapped==logicals[j]){
				j++;
				unwrapped="";
				total++;//改行分
				continue;
			}
			index.push(lineMode?new KILI.Point(unwrapped.length+1,j+1):total);
			if(++found==wrapCount){
				break;
			}
		}
		return index;
	}
	/**
	* 全ての表示行頭の位置を取得する。
	* @param {?Document} $d 対象となるDocument
	* @return {Array}
	*/
	KILI.selex.getViewHeadPos=function($d){
		return KILI.selex.getLogicalHeadPos($d).concat(KILI.selex.getWrapPos($d)).sort(function(a,b){return a-b;});
	}
	/**
	* 全ての表示行末の位置を取得する。
	* @param {?Document} $d 対象となるDocument
	* @return {Array}
	*/
	KILI.selex.getViewEndPos=function($d){
		return KILI.selex.getLogicalEndPos($d).concat(KILI.selex.getWrapPos($d)).sort(function(a,b){return a-b;});
	}
	/**
	* 文字列を含む論理行を取得する。
	* @param {?string|RegExp} seg 検索文字列。指定しない場合は選択部分。
	* 	"``"で囲まれた文字列の場合、鬼車の正規表現で検索する（物凄く遅い）
	* @param {boolean} [noText=false] 文字列を含まない行を取得する
	* @param {boolen} [asText=false] 行番号でなくテキスト自体を返す
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {boolean} [ignoreCase=false] 鬼車で大文字と小文字を区別しない
	* @param {?Document} $d 対象となるDocument
	* @return {Array}
	*/
	KILI.selex.searchTextLines=function(seg,noText,asText,lineMode,ignoreCase,$d){
		$d=GDO($d);
		seg=seg||$d.Selection.Text;
		var result=[];
		if(seg){
			var text=[];
			function splitLine(){
				if(lineMode==KILI.LineMode.view){
					for(var i=0;i<$d.GetLines(meGetLineView);i++){
						text.push($d.GetLine(i,meGetLineView));
					}
				}
				else{
					text=$d.Text.split("\n");
				}
			}
			if(typeof(seg)=="string"){
				if(seg.enclosed("``")){//鬼車
					seg=seg.slice(2,-2);
					var $$d=KILI.docex.newFile($d.Text);
					var $$s=$$d.Selection;
					Redraw=false;
					var flag=(ignoreCase?0:meFindReplaceCase)|meFindReplaceRegExp|meFindNext;
					$$s.SetActivePos(0);
					var found=[];
					lineMode=KILI.LineMode.convert(lineMode);
					while($$s.Find(seg,flag)){
						found.push(asText?$$d.GetLine($$s.GetActivePointY(lineMode)):$$s.GetActivePointY(lineMode));
					}
					if(noText){
						var lines=$$d.GetLines(lineMode);
						for(var i=1,j=0;i<=lines||j<found.length;i++){
							found[j]!=i?result.push(i):j++;
						}
					}
					else{
						result=found;
					}
					KILI.docex.forceClose($$d);
					$d.Activate();
					$d.HighlightFind=false;
					Redraw=true;
				}
				else{//文字列
					splitLine();
					for(var i=0;i<text.length;i++){
						if(text[i].indexOf(seg)>=0^noText){
							result.push(asText?text[i]:i+1);
						}
					}
				}
			}
			else{//正規表現
				splitLine();
				for(var i=0;i<text.length;i++){
					if(seg.test(text[i])^noText){
						result.push(asText?text[i]:i+1);
					}
				}
			}
		}
		return result;
	}
	/**
	* 選択範囲より左の文字列を取得する。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {string}
	*/
	KILI.selex.getLeft=function(lineMode,$d){
		var $s=($d=GDO($d)).Selection;
		var top=KILI.selex.getTopPoint(lineMode,$d);
		return $d.GetLine(top.y,(KILI.LineMode.convert(lineMode))<<1&2).slice(0,top.x-1);
	}
	/**
	* 選択範囲より右の文字列を取得する。
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {string}
	*/
	KILI.selex.getRight=function(lineMode,$d){
		var $s=($d=GDO($d)).Selection;
		var bottom=KILI.selex.getBottomPoint(lineMode,$d);
		return $d.GetLine(bottom.y,(KILI.LineMode.convert(lineMode))<<1&2).slice(bottom.x-1);
	}
	/**
	* 行全体を選択している時、文末側の改行を除外する。
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.selex.excludeBreak=function($d){
		$d=GDO($d);
		if(KILI.selex.wholeLines(KILI.LineMode.logical,$d)==0){
			return;
		}
		var range=KILI.selex.getRange(KILI.Coordinate.pos,$d);
		if(range.direction&&KILI.selex.atLogicalHead(range.bottom,$d)){
			range.direction==1?range.active-=1:range.anchor-=1;
		}
		KILI.selex.setRange(range,null,null,$d);
	}
	/**
	* 検索マッチ数を取得する。
	* @param {string} pattern 検索文字列
	* @param {number} [flag=0] 2:大文字と小文字を区別する、4:単語で検索する、16:正規表現で検索する
	* @param {?Document} $d 対象となるDocument
	* @return {number}
	*/
	KILI.selex.count=function(pattern,flag,$d){
		$d=GDO($d);
		var result=0;
		if(!pattern){
			return result;
		}
		flag=(flag||0)&22;
		Redraw=false;
		var range=KILI.selex.getRange(KILI.Coordinate.logical,$d);
		var scroll=KILI.docex.getScroll();
		var $s=$d.Selection;
		$s.EndOfDocument();
		while($s.Find(pattern,flag)){
			result++;
		}
		$d.HighlightFind=false;
		Redraw=true;
		KILI.selex.setRange(range,null,null,$d);
		KILI.docex.setScroll(scroll);
		return result;
	}
}

if(KILI.edit==null){
	/**
	* Selection拡張。文書を編集するもの。
	* @class
	*/
	KILI.edit={};
	/**
	* テキスト二重化。<br />
	* 無選択時は現在の論理行、複数行選択時は範囲内の論理行、行内選択時は選択部分を二重化する。
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.edit.duplicate=function($d){
		var $s=($d=GDO($d)).Selection;
		var range=KILI.selex.getRange(KILI.Coordinate.logical,$d);
		var scroll=KILI.docex.getScroll();
		if($s.IsEmpty){//無選択
			$s.DuplicateLine();
		}
		else if($s.GetAnchorPointY(mePosLogical)==$s.GetActivePointY(mePosLogical)){//論理行内選択
				$s.Text+=$s.Text;
		}
		else{//複数行選択
			KILI.selex.excludeBreak($d);
			KILI.selex.selectLine($s.GetTopPointY(mePosLogical),$s.GetBottomPointY(mePosLogical),true,KILI.LineMode.logical,$d);
			$s.Text+="\r\n"+$s.Text;
		}
		KILI.selex.setRange(range,null,null,$d);
		KILI.docex.setScroll(scroll);
	}
	/**
	* ソート条件。
	* @class
	* @param {!Function} callback ソート関数
	* @param {boolean} [reverse=false] ソート関数適用後に逆順処理をする
	* @param {string} [separator=""] 分割文字列
	*/
	KILI.edit.criterion=function(callback,reverse,separator){
		this.callback=callback;
		this.reverse=reverse;
		this.separator=separator;
	}
	/**
	* ソート条件を継承して新しい条件を取得する。
	* @static
	* @param {!string} name 元となるソート関数
	* @param {!string} separator 分割文字列
	*/
	KILI.edit.criterion.inherit=function(name,separator){
		var sorter=KILI.edit.lineSorter[name];
		sorter.separator=separator;
		return sorter;
	}
	/**
	* 基本的なソート条件。
	* @class
	*/
	KILI.edit.lineSorter={
		none:new KILI.edit.criterion(function(){return 0;},false,""),
		rev:new KILI.edit.criterion(function(){return 0;},true,""),
		asc:new KILI.edit.criterion(null,false,""),
		desc:new KILI.edit.criterion(null,true,""),
		ascLen:new KILI.edit.criterion(function(a,b){return a.length-b.length;},false,""),
		descLen:new KILI.edit.criterion(function(a,b){return a.length-b.length;},true,"")
	}
	/**
	* 選択文字列ソート。
	* @param {!KILI.edit.criterion} criterion ソート条件
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.edit.sort=function(criterion,$d){
		var targets=$s.Text.split(criterion.separator);
		var range=KILI.selex.getRange(KILI.Coordinate.pos,$d);
		var scroll=KILI.docex.getScroll();
		if(criterion.callback){
			targets.sort(criterion.callback);
		}
		else{
			targets.sort();
		}
		if(criterion.reverse){
			targets.reverse();
		}
		$s.Text=targets.join(criterion.separator);
		KILI.selex.setRange(range.anchor,range.active,null,$d);
		KILI.docex.setScroll(scroll);
	}
	/**
	* 論理行テキストソート。<br />
	* 選択範囲の論理行が対象。無選択の場合は全体。行内での選択の場合は1文字ごとにソートする。
	* @param {number|KILI.edit.criterion} [sorter=0] 0なら逆順、1なら昇順、2なら降順、3なら長さ昇順、4なら長さ降順にソートする。関数の場合はそれを利用する
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.edit.lineSort=function(sorter,$d){
		if(typeof(sorter)=="number"){
			switch(sorter){
				case 1:sorter=KILI.edit.lineSorter.asc;break;
				case 2:sorter=KILI.edit.lineSorter.desc;break;
				case 3:sorter=KILI.edit.lineSorter.ascLen;break;
				case 4:sorter=KILI.edit.lineSorter.descLen;break;
				case 0:
				default:sorter=KILI.edit.lineSorter.rev;
			}
		}
		var $s=($d=GDO($d)).Selection;
		var targets=[];
		KILI.selex.selectIfEmpty();
		var range=KILI.selex.getRange(1,$d);
		var scroll=KILI.docex.getScroll();
		var direction=KILI.selex.getDirection($d);
		if(KILI.selex.atLogicalEnd(range.top,KILI.LineMode.logical)){
			range.top.y+=1;
			range.top.x=1;
		}
		if(range.bottom.x==1){
			range.bottom.y-=1;
			range.bottom.x=$d.GetLine(range.bottom.y,meGetLineLogical).length+1;
		}
		if(range.top.y!=range.bottom.y){
			sorter.separator="\n";
			range=KILI.selex.getRange(KILI.Coordinate.logical,$d);
			KILI.selex.selectLine(range.top.y,range.bottom.y,true,KILI.LineMode.logical,$d);
		}
		KILI.edit.sort(sorter);
		if(sorter.separator=="\n"){
			KILI.selex.selectLine(range.top.y,range.bottom.y,true,KILI.LineMode.logical,$d);
		}
	}
	/**
	* 行を全て削除する。
	* @param {number|Array.<number>} lines 行番号
	* @param {KILI.LineMode} [lineMode=KILI.LineMode.logical] 行モード
	* @param {?Document} $d 対象となるDocument
	* @return {boolean} 削除したかどうか
	*/
	KILI.edit.deleteLines=function(lines,lineMode,$d){
		if(typeof(lines)=="number"){
			lines=[lines];
		}
		if(lines.length){
			$d=GDO($d);
			var text=$d.Text.split("\n");
			var result=[];
			var range=KILI.selex.getRange(KILI.Coordinate.get(lineMode),$d);
			var scroll=KILI.docex.getScroll();
			for(var i=1,j=0;i<=text.length;i++){
				if(j>=lines.length){
					result=result.concat(text.slice(i-1));
					break;
				}
				i==lines[j]?j++:result.push(text[i-1]);
			}
			if(text.length!=result.length){
				$d.Text=result.join("\r\n");
				KILI.selex.setRange(range,null,null,$d);
				KILI.docex.setScroll(scroll);
				return true;
			}
		}
		return false;
	}
	/**
	* 文字列を含む行を全て削除する。
	* @param {?string} seg 削除する文字列。指定しない場合は選択部分
	* @param {boolean} [noText=false] 文字列を含まない行を削除する
	* @param {?Document} $d 対象となるDocument
	* @return {boolean} 置換されたかどうか
	*/
	KILI.edit.deleteAllTextLines=function(seg,noText,$d){
		$d=GDO($d);
		return KILI.edit.deleteLines(KILI.selex.searchTextLines(seg||$d.Selection.Text,noText,false,KILI.LineMode.logical,false,$d));
	}
	/** 
	* 文字列を全て置換する。
	* 処理後スクロール位置などを復元する。
	* @param {string} str 置換後の文字列。指定しない場合は空文字列
	* @param {?string|RegExp} seg 置換する文字列。指定しない場合は選択部分
	* @param {?Document} $d 対象となるDocument
	* @return {boolean} 成功したかどうか
	*/
	KILI.edit.replace=function(str,seg,$d){
		$d=GDO($d);
		var original=$d.Text;
		str=str||"";
		seg=seg||$d.Selection.Text;
		if(seg){
			var range=KILI.selex.getRange(KILI.Coordinate.logical,$d);
			var scroll=KILI.docex.getScroll();
			var converted=original.split(seg).join(str);
			if(converted!=original){
				$s.SelectAll();
				$s.Text=converted;
				range.active=range.anchor;
				KILI.selex.setRange(range,null,null,$d);
				KILI.docex.setScroll(scroll);
				return true;
			}
		}
		return false;
	}
	/**
	* 文字列を全て改行にする。
	* 厳密な改行コードの判定はせずCRLFを用いる。
	* @param {?string} seg 変換する文字列。指定しない場合は選択部分
	* @param {?Document} $d 対象となるDocument
	* @return {boolean} 成功したかどうか
	*/
	KILI.edit.breakAll=function(seg,$d){
		$d=GDO($d);
		return KILI.edit.replace(KILI.docex.getBreakCode($d)||"\r\n",seg,$d);
	}
	/**
	* 文字列を全て削除する。
	* @param {?string} seg 削除する文字列。指定しない場合は選択部分
	* @param {?Document} $d 対象となるDocument
	* @return {boolean} 置換されたかどうか
	*/
	KILI.edit.deleteAll=function(seg,$d){
		return KILI.edit.replace("",seg,$d);
	}
}

if(KILI.docex==null){
	/**
	* Document拡張。Editorの領分と思われるものも含む。
	* @class
	*/
	KILI.docex={};
	/**
	* 全てのDocumentを取得する。
	* @return {Array}
	*/
	KILI.docex.all=function(){
		var result=[];
		var eCount=Editors.Count;
		for(var i=0;i<eCount;i++){
			var editor=Editors.Item(i);
			var dCount=editor.Documents.Count;
			for(var j=0;j<dCount;j++){
				result.push(editor.Documents.Item(j));
			}
		}
		return result;
	}
	/**
	* テキストとスタイルを指定して新規作成する。
	* @param {?string} text
	* @param {Document.Encoding} [encoding=meEncodingNone]
	* @param {Document.Mode} [mode="Text"]
	* @param {Document.ReadOnly} [readOnly=false]
	* @param {Selection.OverwriteMode} [overwrite=false]
	* @param {Editor} [destination] 文書作成先のEditor。指定しなければアクティブ
	* @return {Document} 作成したDocument
	*/
	KILI.docex.newFile=function(text,encoding,mode,readOnly,overwrite,destination){
		destination=GEO(destination);
		Redraw=false;
		destination.NewFile();
		var $d=destination.ActiveDocument;
		$d.Encoding=encoding||meEncodingNone;
		$d.Mode=mode||"Text";
		$d.ReadOnly=readOnly;
		$d.Text=text;
		$d.Selection.OverwriteMode=overwrite;
		Redraw=true;
		return $d;
	}
	/**
	* 文書のクローンを作成する。
	* @param {Editor} [destination] 文書作成先のEditor。指定しなければアクティブ
	* @param {?Document} $d 対象となるDocument
	* @return {Document} 作成したDocument
	*/
	KILI.docex.clone=function(destination,$d){
		destination=GEO(destination);
		$d=GDO($d);
		var range=KILI.selex.getRange(KILI.Coordinate.logical,$d);
		var scroll=KILI.docex.getScroll();
		var $$d=KILI.docex.newFile($d.Text,$d.Encoding,$d.Mode,$d.ReadOnly,$d.Selection.OverwriteMode,destination);
		KILI.selex.setRange(range,null,null,$$d);
		KILI.docex.setScroll(scroll);
		return $$d;
	}
	/**
	* テキストを指定して文書のクローンを作成する。
	* @param {?string} text
	* @param {Editor} [destination] 文書作成先のEditor。指定しなければアクティブ
	* @param {?Document} $d 対象となるDocument
	* @return {Document} 作成したDocument
	*/
	KILI.docex.cloneStyle=function(text,destination,$d){
		destination=GEO(destination);
		$d=GDO($d);
		return KILI.docex.newFile(text,$d.Encoding,$d.Mode,$d.ReadOnly,$d.Selection.OverwriteMode,destination);
	}
	/**
	* 設定ファイルを取得する。
	* @return {string}
	*/
	KILI.docex.getINI=function(){
		var ads=new ActiveXObject("ADODB.Stream")
		var path=(function(){
			var fso=new ActiveXObject("Scripting.FileSystemObject");
			var t=new ActiveXObject("WScript.Shell").SpecialFolders("Appdata")+"\\Mery";
			return fso.FolderExists(t)?t:fso.GetParentFolderName(Editor.FullName);
		})()+"\\Mery.ini";
		ads.Charset="utf-8";
		ads.Open();
		ads.LoadFromFile(path);
		var seg=ads.ReadText().replace(/\r?\n|\r/g,"\n");
		ads.Close();
		return seg;
	}
	/**
	* 文書に変更があっても強制的に閉じる。
	* @param {number} saved -1:ディスク上に存在しないファイルのみ、0:全て、1:存在するファイルのみ。
	* @param {?Document} $d 対象となるDocument
	*/
	KILI.docex.forceClose=function(saved,$d){
		$d=GDO($d);
		var flag=false;
		if(
			saved==0||
			saved==-1&&$d.FileName==""||
			saved==1&&$d.FileName!=""
		){
			flag=true;
		}
		if(flag){
			$d.Saved=true;
			$d.Close();
		}
	}
	/**
	* スクロールバーの位置を取得する。
	* @return {KILI.Point}
	*/
	KILI.docex.getScroll=function(){
		return new KILI.Point(ScrollX,ScrollY);
	}
	/**
	* スクロールバーの位置を設定する。
	* @param {number|KILI.Point} x 横スクロールバーの位置または両スクロールバーの位置
	* @param {number} y 縦スクロールバーの位置
	*/
	KILI.docex.setScroll=function(x,y){
		if(x instanceof KILI.Point){
			y=x.y;
			x=x.x;
		}
		Redraw=false;
		ScrollX=x;
		ScrollY=y;
		Redraw=true;
	}
	/**
	* 改行文字を取得する。
	* @param {Document} $d 対象となるDocument
	* @param {boolean} [all=false] ディスク上に存在しないファイルも判定する（ファイルを保存するので、Document.FullName等が変更される）
	* @param {boolean} [useUndo=false] 改行が含まれていない場合に備えて、改行を書き込んで判定する（Undoバッファが汚染される）
	* @return {string} 改行文字。不明の場合は空文字列
	*/
	KILI.docex.getBreakCode=function($d,all,useUndo){
		var exist=$d.FullName!="";
		if(!all&&!exist){
			return "";
		}
		Redraw=false;
		var fso=new ActiveXObject("Scripting.FileSystemObject");
		var path=$d.FullName||fso.BuildPath(fso.GetParentFolderName(Editor.FullName),"tmp\\無題");
		var saved=$d.Saved;
		$d.Save(path);
		$d.Saved=saved;
		if(useUndo){
			var range=KILI.selex.getRange(KILI.Coordinate.pos,$d);
			var scroll=KILI.docex.getScroll();
			$d.Selection.NewLine();
			$d.Save(path);
			$d.Undo();
			//$d.Selection.Text=$d.Selection.Text;
			if(saved){
				$d.Save(path);
			}
			KILI.selex.setRange(range,null,null,$d);
			KILI.docex.setScroll(scroll);
		}
		var ads=new ActiveXObject("ADODB.Stream");
		ads.Charset="_autodetect_all";
		ads.Open();
		ads.LoadFromFile(path);
		var seg=ads.ReadText();
		ads.Close();
		if(!exist){
			fso.DeleteFile(path,true);
		}
		Redraw=true;
		return seg.match(/\r\n|\r|\n/)?RegExp.lastMatch:"";
	}
}

//--------------------------------------------------------------------
//Stringオブジェクト拡張。Meryの機能を使う為StringEx.jsからは独立している。
//--------------------------------------------------------------------
/**
* 擬似的に鬼車で検索する。
* @param {string} pattern 検索パターン
* @param {boolean} [ignoreCase=false] 大文字と小文字を区別しない
* @param {number} [max=Number.MAX_VALUE] マッチの最大数
* @return {Array.<Array.<number,string>>} マッチしたインデックスとマッチした文字列のペア
*/
String.prototype.onigFind=function(pattern,ignoreCase,max){
	var $d=GDO();
	var result=[];
	if(!this||!pattern){
		return result;
	}
	max=max||Number.MAX_VALUE;
	var $$d=KILI.docex.newFile(this);
	var $$s=$$d.Selection;
	Redraw=false;
	$$s.SetActivePos(0);
	var flag=(ignoreCase?0:meFindReplaceCase)|meFindReplaceRegExp|meFindNext;
	while($$s.Find(pattern,flag)){
		result.push([$$s.GetActivePos(),$$s.Text]);
		if(result.length==max){
			break;
		}
	}
	$$d.Close();
	$d.Activate();
	$d.HighlightFind=false;
	Redraw=true;
	return result;
};
