#title "文字コード整形"
#include "StringEx.js"
#include "NumberEx.js"

if(KILI.code==null){
	/**
	* 文字・コードポイントの相互変換とその表現を扱う。Unicodeのみが対象。<br />
	* 表現方法は以下の様に対応する。
	* <dl>
	* <dt>Unicode形式</dt><dd>U+hhhh</dd>
	* <dt>正規表現形式</dt><dd>\uhhhh</dd>
	* <dt>鬼車形式</dt><dd>\x{hhhh}</dd>
	* <dt>数値実体参照（十六進）</dt><dd>&#xhhhh</dd>
	* <dt>数値実体参照（十進）</dt><dd>&#dddd</dd>
	* </dl>
	* @class
	*/
	KILI.code={};
	/**
	* Unicode形式パターン。
	* @const
	*/
	var unicodePat=/U\+([\da-fA-F]{2,6})/g;
	/**
	* 正規表現パターン。
	* @const
	*/
	var regexPat=/\\u([\da-fA-F]{2,6})/g;
	/**
	* 鬼車形式パターン。
	* @const
	*/
	var onigPat=/\\x\{([\da-fA-F]{2,6})\}/g;
	/**
	* 数値実体参照（十六進）パターン。
	* @const
	*/
	var entityHexPat=/&#x([\da-fA-F]{2,6});/g;
	/**
	*数値実体参照（十進）パターン。
	* @const
	*/
	var entityDecPat=/&#([\d]{2,6});/g;
	/**
	* 数値実体参照パターン。
	* @const
	*/
	var entityPat=/&#x([\da-fA-F]{2,6});|&#(\d{2,6});/g;
	/**
	* 全ての形式に対応したパターン。
	* @const
	*/
	var escapePat=/(?:U\+|\\u)([\da-fA-F]{2,6})|\\x\{([\da-fA-F]{2,6})\}|&#x([\da-fA-F]{2,6});|&#(\d{2,6});/g;
	
	/**
	* 文字列をコードポイントの配列に変換する。
	* @param {string} seg
	* @param {boolean} [surrogate=false] サロゲートペアで出力する
	* @param {number} [alignmentString=0] 指定した桁数の揃える。0以下ならば揃えない
	* @param {boolean} [hex=false] 十六進数文字列で返す
	* @return {Array}
	*/
	KILI.code.encode=function(seg,surrogate,alignmentString,hex){
		var result=[];
		for(var i=0;i<seg.length;i++){
			var t=seg.charCodeAt(i);
			var next=seg.charCodeAt(i+1);
			if(t.within(0xD800,0xDBFF)&&next.within(0xDC00,0xDFFF)){
				i++;
				if(!surrogate){
					t=KILI.code.decodeSurrogate(t,next);
				}
				else{
					if(hex){
						t=t.toString(16);
						next=next.toString(16);
					}
					if(alignmentString>0){
						t=Number.prototype.padding.apply(t,[alignmentString,"0"]);
						next=Number.prototype.padding.apply(next,[alignmentString,"0"]);
					}
					result.push(t,next);
					continue;
				}
			}
			if(hex){
				t=t.toString(16);
			}
			if(alignmentString>0){
				t=Number.prototype.padding.apply(t,[alignmentString,"0"]);
			}
			result.push(t);
		}
		return result;
	}
	/**
	* コードポイントの配列を文字列に変換する。
	* @param {Array} code 文字コードの配列
	* @return {String}
	*/
	KILI.code.decode=function(code){
		var result="";
		for(var i=0;i<code.length;i++){
			result+=KILI.code.fromCharCode(code[i]);
		}
		return result;
	}
	/**
	* 0x10000以上にも対応したString.fromCharCode。
	* @param {...number} args
	* @return {String}
	*/
	KILI.code.fromCharCode=function(){
		var result=[];
		for(var i=0;i<arguments.length;i++){
			if(arguments[i]<0x10000){
				result.push(arguments[i]);
			}
			else{
				result=result.concat(KILI.code.encodeSurrogate(arguments[i]));
			}
		}
		return String.fromCharCode.apply(null,result);
	}
	/**
	* コードポイントをサロゲートペアに変換する。
	* @param {number} code
	* @return {Array|number} コードポイントが0x10000未満の場合そのまま返す
	*/
	KILI.code.encodeSurrogate=function(code){
		if(code<0x10000){
			return code;
		}
		code-=0x10000;
		return [0xd800|code>>10,0xdc00|code&0x3ff];
	}
	/**
	* サロゲートペアをコードポイントに変換する。
	* @param {number} higher 上位サロゲート
	* @param {number} lower 下位サロゲート
	* @return {number}
	*/
	KILI.code.decodeSurrogate=function(higher,lower){
		return ((higher&0x3ff)<<10|lower&0x3ff)+0x10000;
	}
	/**
	* Unicode形式を文字に変換する。
	* @param {String} seg
	* @return {String}
	*/
	KILI.code.fromUnicode=function(seg){
		return seg.replace(unicodePat,function($1,$2){
			return KILI.code.fromCharCode(parseInt($2,16));
		});
	}
	/**
	* 文字列をUnicode形式に変換する。
	* @param {String} seg
	* @param {boolelan} [surrogate=false] サロゲートペアで出力する
	* @param {number} [alignmentString=0] 指定した桁数に揃える。0以下ならば揃えない
	* @return {String}
	*/
	KILI.code.toUnicode=function(seg,surrogate,alignmentString){
		return "U+"+KILI.code.encode(seg,surrogate,alignmentString,true).join("U+");
	}
	/**
	* 正規表現形式を文字に変換する。
	* @param {String} seg
	* @return {String}
	*/
	KILI.code.fromRegex=function(seg){
		return seg.replace(regexPat,function($1,$2){
			return KILI.code.fromCharCode(parseInt($2,16));
		});
	}
	/**
	* 文字列を正規表現形式に変換する。
	* @param {String} seg
	* @param {boolelan} [surrogate=false] サロゲートペアで出力する
	* @param {number} [alignmentString=0] 指定した桁数に揃える。0以下ならば揃えない
	* @return {String}
	*/
	KILI.code.toRegex=function(seg,surrogate,alignmentString){
		return "\\u"+KILI.code.encode(seg,surrogate,alignmentString,true).join("\\u");
	}
	/**
	* 鬼車形式を文字に変換する。
	* @param {String} seg
	* @return {String}
	*/
	KILI.code.fromOnig=function(seg){
		return seg.replace(onigPat,function($1,$2){
			return KILI.code.fromCharCode(parseInt($2,16));
		});
	}
	/**
	* 文字列を鬼車形式に変換する。
	* @param {String} seg
	* @param {boolelan} [surrogate=false] サロゲートペアで出力する
	* @param {number} [alignmentString=0] 指定した桁数に揃える。0以下ならば揃えない
	* @return {String}
	*/
	KILI.code.toOnig=function(seg,surrogate,alignmentString){
		return KILI.code.encode(seg,surrogate,alignmentString,true).join("}\\x{").enclose("\\x{","}");
	}
	/**
	* 数値実体参照を文字に変換する。
	* @param {String} seg
	* @param {boolean} [radix=null] trueならば十進数のみ、falseならば十六進数のみを変換する。nullならば両方変換する
	* @return {String}
	*/
	KILI.code.fromEntity=function(seg,radix){
		if (radix==undefined){
			return seg.replace(entityPat,function($1,$2,$3){
				return KILI.code.fromCharCode($3?parseInt($3,10):parseInt($2,16));
			});
		}
		else{
			var pat=radix?entityDecPat:entityHexPat;
			var radix=radix?10:16;
			return seg.replace(pat,function($1,$2){O(radix)
				return KILI.code.fromCharCode(parseInt($2,radix));
			});
		}
	}
	/**
	* 文字列を数値実体参照に変換する。
	* @param {String} seg
	* @param {boolelan} [surrogate=false] サロゲートペアで出力する
	* @param {number} [alignmentString=0] 指定した桁数に揃える。0以下ならば揃えない
	* @param {boolean} [radix=false] trueならば十進数表記、falseならば十六進数表記にする
	* @return {String}
	*/
	KILI.code.toEntity=function(seg,surrogate,alignmentString,radix){
		var prefix=radix?"&#":"&#x";
		return KILI.code.encode(seg,surrogate,alignmentString,!radix).join(";"+prefix).enclose(prefix,";");
	}
	/**
	* 文字列中のコードポイント表現を文字列にする。<br />
	* @return {String}
	*/
	KILI.code.decodeAll=function(seg){
		return seg.replace(escapePat,function($1,$2,$3,$4,$5){
			return KILI.code.decode([$5?$5:parseInt($2||$3||$4,16)]);
		});
	}
}
