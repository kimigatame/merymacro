#title="マウス操作"
#include "StringEx.js"
#include "NumberEx.js"
////////////////////////////////////////////////////////////////////////////////
//■概要:
//マウスの操作や座標の取得をする。
////////////////////////////////////////////////////////////////////////////////

/**
* マウスの操作や座標の取得をする。
* @class
*/
function MouseEmulator(){
	var ws=new ActiveXObject("WScript.Shell");
	var fso=new ActiveXObject("Scripting.FileSystemObject");
	var dll=fso.BuildPath(ws.CurrentDirectory,"Macros\\kili\\Mouse.dll");
	var exist=fso.FileExists(dll);
	var cmd="rundll32.exe "+dll.enclose('"')+",";
	/**
	* ウィンドウの位置を取得する。
	* @return {MouseEmulator.Rect}
	*/
	this.window=function(){
		if(exist){
			var pos=ws.exec(cmd+"_PrintWinSize@16");
			while(pos.Status==0){
				Sleep(10);
			}
			var o=eval(pos.StdOut.ReadAll().enclose("(",")"));
			return new MouseEmulator.Rect(obj.left,obj.top,obj.right,obj.bottom);
		}
		else{
			return new MouseEmulator.Rect(-1,-1,-1,-1);
		}
	}
	/**
	* ウィンドウ内での座標を取得する。
	* @return {MouseEmulator.Point}
	*/
	this.getWindowXY=function(){
		if(exist){
			var pos=this.window();
			var xy=this.getXY();
			return new MouseEmulator.Point(xy.x-pos.left,xy.y-pos.top);
		}
		else{
			return new MouseEmulator.Point(-1,-1);
		}
	}
	/**
	* ウィンドウ内でのX座標を取得する。
	* @return {number}
	*/
	this.getWindowX=function(){
		return exist?this.getWindowXY().x:-1;
	}
	/**
	* ウィンドウ内でのY座標を取得する。
	* @return {number}
	*/
	this.getWindowY=function(){
		return exist?this.getWindowXY().y:-1;
	}
	/**
	* ウィンドウ内での座標を設定する。
	* @param {number|MouseEmulator.Point} x X座標もしくは{@link MouseEmulator.Point}
	* @param {number} y Y座標
	*/
	this.setWindowXY=function(x,y){
		if(exist){
			if(typeof(x)=="object"){
				y=x.y;
				x=x.x;
			}
			var pos=this.window();
			ws.run(cmd+"_SetXY@16 "+(pos.left+x).constrain(pos.left,pos.right)+","+(pos.top+y).constrain(pos.top,pos.bottom),0,true);
		}
	}
	/**
	* ウィンドウ内でのX座標を設定する。
	* @param {number} x X座標
	*/
	this.setWindowX=function(x){
		if(exist){
			var pos=this.window();
			ws.run(cmd+"_SetX@16 "+(pos.left+x).constrain(pos.left,pos.right),0,true);
		}
	}
	/**
	* ウィンドウ内でのY座標を設定する。
	* @param {number} y Y座標
	*/
	this.setWindowY=function(y){
		if(exist){
			var pos=this.window();
			ws.run(cmd+"_SetY@16 "+(pos.top+y).constrain(pos.top,pos.bottom),0,true);
		}
	}
	/**
	* 座標を設定する。
	* @param {number|MouseEmulator.Point} x X座標もしくは{@link MouseEmulator.Point}
	* @param {number} y Y座標
	*/
	this.setXY=function(x,y){
		if(exist){
			if(typeof(x)=="object"){
				y=x.y;
				x=x.x;
			}
			ws.run(cmd+"_SetXY@16 "+x+","+y,0,true);
		}
	}
	/**
	* X座標を設定する。
	* @param {number} x X座標
	*/
	this.setX=function(x){
		if(exist){
			ws.run(cmd+"_SetX@16 "+x,0,true);
		}
	}
	/**
	* Y座標を設定する。
	* @param {number} y Y座標
	*/
	this.setY=function(y){
		if(exist){
			ws.run(cmd+"_SetY@16 "+y,0,true);
		}
	}
	/**
	* 座標を取得する。
	* @return {MouseEmulator.Point}
	*/
	this.getXY=function(){
		if(exist){
			var pxy=ws.exec(cmd+"_PrintXY@16");
			while(pxy.Status==0){
				Sleep(10);
			}
			var o=eval(pxy.StdOut.ReadAll().enclose("(",")"));
			return new MouseEmulator.Point(o.x,o.y);
		}
		else{
			return new MouseEmulator.Point(-1,-1);
		}
	}
	/**
	* X座標を取得する。
	* @return {number}
	*/
	this.getX=function(){
		if(exist){
			var pxy=ws.exec(cmd+"_PrintXY@16");
			while(pxy.Status==0){
				Sleep(10);
			}
			return eval(pxy.StdOut.ReadAll().enclose("(",")")).x;
		}
		else{
			return -1;
		}
	}
	/**
	* Y座標を取得する。
	* @return {number}
	*/
	this.getY=function(){
		if(exist){
			var pxy=ws.exec(cmd+"_PrintXY@16");
			while(pxy.Status==0){
				Sleep(10);
			}
			return eval(pxy.StdOut.ReadAll().enclose("(",")")).y;
		}
		else{
			return -1;
		}
	}
	/**
	* 座標を移動する。
	* @param {number} dx X座標の移動量
	* @param {number} dy Y座標の移動量
	*/
	this.moveXY=function(dx,dy){
		if(exist){
			ws.run(cmd+"_MoveXY@16 "+dx+","+dy,0,true);
		}
	}
	/**
	* X座標を移動する。
	* @param {number} dx X座標の移動量
	*/
	this.moveX=function(dx){
		if(exist){
			ws.run(cmd+"_MoveX@16 "+dx,0,true);
		}
	}
	/**
	* Y座標を移動する。
	* @param {number} dy Y座標の移動量
	*/
	this.moveY=function(dx){
		if(exist){
			ws.run(cmd+"_MoveY@16 "+dx,0,true);
		}
	}
	/**
	* 左ボタンを下げる。
	*/
	this.downLeft=function(){
		if(exist){
			ws.run(cmd+"_DownLeft@16",0,true);
		}
	}
	/**
	* 左ボタンを上げる。
	*/
	this.upLeft=function(){
		if(exist){
			ws.run(cmd+"_UpLeft@16",0,true);
		}
	}
	/**
	* 左クリック。
	*/
	this.clickLeft=function(){
		if(exist){
			ws.run(cmd+"_ClickLeft@16",0,true);
		}
	}
	/**
	* 左ダブルクリック。
	*/
	this.doubleClickLeft=function(){
		if(exist){
			ws.run(cmd+"_DoubleClickLeft@16",0,true);
		}
	}
	/**
	* 右ボタンを下げる。
	*/
	this.downRight=function(){
		if(exist){
			ws.run(cmd+"_DownRight@16",0,true);
		}
	}
	/**
	* 右ボタンを上げる。
	*/
	this.upRight=function(){
		if(exist){
			ws.run(cmd+"_UpRight@16",0,true);
		}
	}
	/**
	* 右クリック。
	*/
	this.clickRight=function(){
		if(exist){
			ws.run(cmd+"_ClickRight@16",0,true);
		}
	}
	/**
	* 右ダブルクリック。
	*/
	this.doubleClickRight=function(){
		if(exist){
			ws.run(cmd+"_DoubleClickRight@16",0,true);
		}
	}
	/**
	* 中ボタン下げる。
	*/
	this.downMiddle=function(){
		if(exist){
			ws.run(cmd+"_DownMiddle@16",0,true);
		}
	}
	/**
	* 中ボタン上げる。
	*/
	this.upMiddle=function(){
		if(exist){
			ws.run(cmd+"_UpMiddle@16",0,true);
		}
	}
	/**
	* 中ボタンクリック。
	*/
	this.clickMiddle=function(){
		if(exist){
			ws.run(cmd+"_ClickMiddle@16",0,true);
		}
	}
	/**
	* 中ボタンダブルクリック。
	*/
	this.doubleClickMiddle=function(){
		if(exist){
			ws.run(cmd+"_DoubleClickMiddle@16",0,true);
		}
	}
	/**
	* ホイールを回転させる。
	* @param {number} delta ホイールの回転量
	*/
	this.rotWheel=function(delta){
		if(exist){
			ws.run(cmd+"_RotWheel@16 "+delta,0,true);
		}
	}
}
/**
* 領域を表す。
* @class
* @param {number} top
* @param {number} bottom
* @param {number} left
* @param {number} right
*/
MouseEmulator.Rect=function(left,top,right,bottom){
	this.top=top;
	this.bottom=bottom;
	this.left=left;
	this.right=right;
}
/**
* 座標を表す。
* @class
* @param {number} x
* @param {number} y
*/
MouseEmulator.Point=function(x,y){
	this.x=x;
	this.y=y;
}
