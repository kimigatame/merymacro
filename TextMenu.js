#title="テキスト操作"
#include "kili/KILI.js"
#include "kili/WidePop.js"
#include "kili/KILI.code.js"
#include "kili/StringEx.js"

/**
* テキストをいろいろと操作する。
*/
var $d=GDO();
var $s=GSO();
var menu=[];
menu.push(["空（白のみの）行を削除(&B)",1,function(){
	var pat=/^[\s\u0020\u00a0\u2000-\u200b\u2028-\u3000]*\n/gm;
	$d.Text=$d.Text.replace(pat,"").replace(/\n$/,"").crlf();
}
]);
menu.push(["重複行を削除(&L)",1,function(){$d.Text=$d.Text.split("\n").undup().join("\r\n");}]);
menu.push(["隣り合う重複行を削除(&N)",1,function(){$s.Replace("^(.+)(?:\\n\\1)+$","$1",meFindReplaceRegExp|meReplaceAll);}]);
menu.push([]);
menu.push(["二重化(&D)",1,KILI.edit.duplicate]);
menu.push(["バックスラッシュをエスケープ(&E)",1,function(){
	if(/\\/.test($s.Text)){
		$s.Replace("\\","\\\\",meReplaceSelOnly|meReplaceAll);
		$d.HighlightFind=false;
	}
},null,1]);
menu.push(["選択部分(&T)",1,null,[
	["全て新規テキストへ(&A)",1,function(){KILI.docex.cloneStyle($s.Text);}],
	["含む行を新規テキストへ(&C)",1,function(){
		KILI.docex.cloneStyle(KILI.selex.searchTextLines($s.Text,false,true).join("\n"));
	}],
	["含まない行を新規テキストへ(&N)",1,function(){
		KILI.docex.cloneStyle(KILI.selex.searchTextLines($s.Text,true,true).join("\n"));
	}],
	["始まる行を新規テキストへ(&S)",1,function(){
		KILI.docex.cloneStyle(KILI.selex.searchTextLines(new RegExp("^"+$s.Text.escapeRegExp(),"g"),false,true).join("\n"));
	}],
	["終わる行を新規テキストへ(&E)",1,function(){
		KILI.docex.cloneStyle(KILI.selex.searchTextLines(new RegExp($s.Text.escapeRegExp()+"$","g"),false,true).join("\n"));
	}],
	[],
	["全て改行(&B)",1,function(){KILI.edit.breakAll()}],
	["全て削除(&D)",1,function(){KILI.edit.deleteAll()}],
	["含む行を削除(&H)",1,function(){KILI.edit.deleteAllTextLines()}],
	["含まない行を削除(&O)",1,function(){KILI.edit.deleteAllTextLines($s.Text,true);}],
	["始まる行を削除(&K)",1,function(){KILI.edit.deleteAllTextLines(new RegExp("^"+$s.Text.escapeRegExp()));}],
	["終わる行を削除(&J)",1,function(){KILI.edit.deleteAllTextLines(new RegExp($s.Text.escapeRegExp()+"$"));}],
	["左トリム(&L)",1,function(){KILI.edit.replace("",new RegExp("^("+$s.Text.escapeRegExp()+")+","m"));}],
	["右トリム(&R)",1,function(){KILI.edit.replace("",new RegExp("("+$s.Text.escapeRegExp()+")+"+"$","m"));}]
],1]);
menu.push(["囲む(&Q)",1,null,[
	[" ( ) ",1,function(pm){$s.Text=$s.Text.enclose("(",")",pm)}],
	[" [ ] ",1,function(){$s.Text=$s.Text.enclose("[","]");}],
	[" ' ' ",1,function(){$s.Text=$s.Text.enclose("'","'");}],
	[" \" \" ",1,function(){$s.Text=$s.Text.enclose("\"","\"");}],
	[" { } ",1,function(){$s.Text=$s.Text.enclose("{","}");}],
	[" < > ",1,function(){$s.Text=$s.Text.enclose("<",">");}],
	[" （ ） ",1,function(){$s.Text=$s.Text.enclose("（","）");}],
	[" ［ ］ ",1,function(){$s.Text=$s.Text.enclose("［","］");}],
	[" 「 」 ",1,function(){$s.Text=$s.Text.enclose("「","」");}],
	[" 『 』 ",1,function(){$s.Text=$s.Text.enclose("『","』");}],
	[" 【 】 ",1,function(){$s.Text=$s.Text.enclose("【","】");}],
	[" /* */ ",1,function(){$s.Text=$s.Text.enclose("/*","*/");}]
]]);
menu.push([]);
menu.push(["コード変換(&C)",1,null,[
	["Unicode(&U)",1,null,[
		["文字 → U+????(&E)",1,function(){$s.Text=KILI.code.toUnicode($s.Text,false,4);}],
		["文字 → U+????U+????（サロゲートペア）(&S)",1,function(){$s.Text=KILI.code.toUnicode($s.Text,true,4);}],
		["全て → 文字(&D)",1,function(){$s.Text=KILI.code.fromUnicode($s.Text);}]
	]],
	["正規表現(&R)",1,null,[
		["文字 → \\u????(&E)",1,function(){$s.Text=KILI.code.toRegex($s.Text,true,4);}],
		["\\u???? → 文字(&D)",1,function(){$s.Text=KILI.code.fromRegex($s.Text);}]
	]],
	["実体参照(&E)",1,null,[
		["文字 → &&#x????(&H);",1,function(){$s.Text=KILI.code.toEntity($s.Text,false,4);}],
		["文字 → &&#????(&D);",1,function(){$s.Text=KILI.code.toEntity($s.Text,false,4,true);}],
		["&&#x????; → 文字(&X)",1,function(){$s.Text=KILI.code.fromEntity($s.Text,false);}],
		["&&#????; → 文字(&C)",1,function(){$s.Text=KILI.code.fromEntity($s.Text,true);}],
		["全て → 文字(&A)",1,function(){$s.Text=KILI.code.fromEntity($s.Text);}]
	]],
	["鬼車(&O)",1,null,[
		["文字 → \\x{????}(&E)",1,function(){$s.Text=KILI.code.toOnig($s.Text,false,4);}],
		["\\x{????} → 文字(&D)",1,function(){$s.Text=KILI.code.fromOnig($s.Text);}]
	]],
	["全て → 文字(&D)",1,function(){$s.Text=KILI.code.decodeAll($s.Text);}]
],1]);
menu.push([]);
menu.push(["ソート(&S)",1,null,[
	["逆順(&R)",1,function(){KILI.edit.lineSort(0);}],
	["昇順(&A)",1,function(){KILI.edit.lineSort(1);}],
	["降順(&D)",1,function(){KILI.edit.lineSort(2);}],
	["長さ昇順(&L)",1,function(){KILI.edit.lineSort(3);}],
	["長さ降順(&S)",1,function(){KILI.edit.lineSort(4);}]
]]);
menu.push(["「,」の前後でソート(&S)",1,null,[
	["逆順(&R)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("rev",","));}],
	["昇順(&A)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("asc",","));}],
	["降順(&D)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("desc",","));}],
	["長さ昇順(&L)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("ascLen",","));}],
	["長さ降順(&S)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("descLen",","));}]
],1]);
menu.push(["タブの前後でソート(&S)",1,null,[
	["逆順(&R)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("rev",","));}],
	["昇順(&A)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("asc",","));}],
	["降順(&D)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("desc",","));}],
	["長さ昇順(&L)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("ascLen",","));}],
	["長さ降順(&S)",1,function(){KILI.edit.sort(KILI.edit.criterion.inherit("descLen",","));}]
],1]);
var popup=new WidePop(menu);
if($s.IsEmpty){
	popup.enable(0,function(pop,me){return me.tags==1;})
}
popup.create().track(1,false);