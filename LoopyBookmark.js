#title="ブックマーク移動"

/**
* 最後のブックマークまで来たら最初のブックマークへ移動する。
* @param {boolean} [backward=false] 文頭方向へ移動する
*/
var LoopyBookmark=function(backward){
	var $s=Document.Selection;
	if(backward==undefined){
		backward=false
	}
	if(backward){
		if(!$s.PreviousBookmark()){
			$s.EndOfDocument();
			$s.PreviousBookmark();
		}
	}
	else{
		if(!$s.NextBookmark()){
			$s.StartOfDocument();
			$s.NextBookmark();
		}
	}
}
