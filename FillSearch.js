#title="はめ込み検索"
#include "kili/KILI.js"
#include "kili/StringEx.js"
#include "kili/WidePop.js"
#include "kili/json2.js"

/**
* 選択部分をテンプレートにはめ込んで作成した文字列で検索する。<br />
* 「<##>」が選択文字列に置き換えられる（{@link FillSearch~here}で変更可能）。<br />
* リストファイルはJSONで、形式は「"entries":[FillSearch.Entry...]」。サンプルファイルを参照。
* @class
* @param {string} path リストファイルのパス
*/
function FillSearch(path){
	/**
	* リスト本体。
	* @type {Array.<FillSearch.Entry>}
	*/
	var list;
	/**
	* 補完時の選択文字列を挿入する位置のアンカー。
	* @type {string}
	*/
	var here="<##>";
	/**
	* 検索を実行する。
	*/
	this.execute=function(){
		var menu=[];
		var t=list.select(function(index,value){
			return value.template==undefined||value.modes.length==0||value.modes==GDO().Mode;
		});
		for(var i=0;i<t.length;i++){
			if(t[i].template==undefined){
				if(menu.length&&menu.last().length>0&&i<t.length-1){
					menu.push([]);
				}
			}
			else{
				menu.push([t[i].name,1,function(pop,me){search(me.tags[0],me.tags[1]);},null,[t[i].template,t[i].flags]]);
			}
		}
		new WidePop(menu).create().track(1,false);
	};
	/**
	* 検索本体。
	* @param {string} template 正規表現
	* @param {?number} flag Selection.Findで使うフラグ
	*/
	var search=function(template,flag){
		var $s=GSO();
		if(flag==null){
			flag=meFindNext|meFindReplaceCase;
		}
		var pat=$s.Text;
		if(flag&16){
			pat=pat.escapeRegExp();
		}
		$s.Find(template.replace(new RegExp(here,"g"),pat),flag);
	};

	var file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(path,1,true,-1);
	if(!file.AtEndOfStream){
		list=JSON.parse(file.ReadAll())["entries"];
	}
	file.Close();
}
/**
* 項目。
*/
FillSearch.Entry=function(){
	/**
	* 項目名。
	* @type {string}
	*/
	this.name;
	/**
	* 検索パターン。
	* @type {string}
	*/
	this.template;
	/**
	* 検索フラグ。
	* @type {number}
	* <table>
	* <tr><td>0</td><td>前を検索</td></tr>
	* <tr><td>1</td><td>次を検索</td></tr>
	* <tr><td>2</td><td>大小文字を区別する</td></tr>
	* <tr><td>4</td><td>単語のみ検索する</td></tr>
	* <tr><td>8</td><td>文末まで検索したら文頭へ移動する</td></tr>
	* <tr><td>16</td><td>正規表現で検索する</td></tr>
	* </table>
	*/
	this.flags;
	/**
	* 対応する編集モード。空の配列ならば全ての編集モードに対応する
	* @type {Array.<string>}
	*/
	this.modes;
}
