#title="選択部分を移動"
#include "kili/KILI.js"

/**
* 選択部分を移動する。
* @class
* @param {number} dir 移動方向。4:左、6:右、8:上、2:下
*/

var MoveSelection=function(dir){
	var $d=GDO();
	var $s=$d.Selection;
	var initialActive=$s.GetActivePos();
	var initialAnchor=$s.GetAnchorPos();
	var endActive=initialActive;
	var endAnchor=initialAnchor;
	var selDirection=KILI.selex.getDirection();
	var range=KILI.selex.getRange(KILI.Coordinate.view);
	var selLength=$s.Text.length;
	switch(dir){
		case 4:
			if(selDirection==KILI.Direction.forward){
				if(initialAnchor==0){
					break;
				}
				initialAnchor--;
				endActive--;
				endAnchor--;
			}
			else if(selDirection==KILI.Direction.backward){
				if(initialActive==0){
					break;
				}
				initialActive--;
				endActive--;
				endAnchor--;
			}
			else{
				break;
			}
			$s.SetAnchorPos(initialAnchor);
			$s.SetActivePos(initialActive,true);
			$s.Text=$s.Text.slice(1)+$s.Text.slice(0,1);
			$s.SetAnchorPos(endAnchor);
			$s.SetActivePos(endActive,true);
			break;
		case 6:
			if(selDirection==KILI.Direction.forward){
				if(initialActive==$d.Text.length){
					break;
				}
				initialActive++;
				endActive++;
				endAnchor++;
			}
			else if(selDirection==KILI.Direction.backward){
				if(initialAnchor==$s.Text.length){
					break;
				}
				initialAnchor++;
				endActive++;
				endAnchor++;
			}
			else{
				break;
			}
			$s.SetAnchorPos(initialAnchor);
			$s.SetActivePos(initialActive,true);
			$s.Text=$s.Text.slice(-1)+$s.Text.slice(0,-1);
			$s.SetAnchorPos(endAnchor);
			$s.SetActivePos(endActive,true);
			break;
		case 8:
			if(range.top.y==1){
				break;
			}
			if(selDirection==KILI.Direction.forward){
				range.anchor.y--;
				endAnchor=KILI.selex.viewToPos(range.anchor);
				endActive=endAnchor+selLength;
			}
			else if(selDirection==KILI.Direction.backward){
				range.active.y--;
				endActive=KILI.selex.viewToPos(range.active);
				endAnchor=endActive+selLength;
			}
			else{
				break;
			}
			KILI.selex.setRange(range);
			$s.Text=$s.Text.slice(-selLength)+$s.Text.slice(0,-selLength);
			$s.SetAnchorPos(endAnchor);
			$s.SetActivePos(endActive,true);
			break;
		case 2:
			if(range.bottom.y==$d.GetLine(meGetLineView)){
				break;
			}
			if(selDirection==KILI.Direction.forward){
				range.active.y++;
				range.active.x=range.anchor.x;
				endAnchor=KILI.selex.viewToPos(range.active)-selLength;
				endActive=endAnchor+selLength;
			}
			else if(selDirection==KILI.Direction.backward){
				range.anchor.y++;
				range.anchor.x=range.active.x;
				endActive=KILI.selex.viewToPos(range.anchor)-selLength;
				endAnchor=endActive+selLength;
			}
			else{
				break;
			}
			KILI.selex.setRange(range);
			$s.Text=$s.Text.slice(selLength)+$s.Text.slice(0,selLength);
			$s.SetAnchorPos(endAnchor);
			$s.SetActivePos(endActive,true);
			break;
		default:
	}
}