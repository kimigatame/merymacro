#title="スニペットダイアログ"
#include "../InsetText.js"
#include "../kili/WidePop.js"

var r=new InsetText(ScriptFullName.split("\\").slice(0,-1).join("\\")+"\\insettext.json").match(false);
if(r.generated.length>1){
	var menu=[];
	for(var i=0;i<r.generated.length;i++){
		menu.push([r.generated[i].name,r.generated[i]]);
	}
	menu=WidePop.normalize(menu);
	var result=new WidePop(menu).create().track(0,false).value;
	if(result){
		result.insert();
	}
}
else{
	r.generated[0].insert();
}
