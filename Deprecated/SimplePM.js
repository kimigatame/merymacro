#title="単純ポップアップニュー"
#include "KILI.js"

/**
* テキストと実行コマンドのみからなる単純なPopupMenu。
* @class
* @param {SimpleMenu~menuDefinition} menu メニューの定義
* 
* @deprecated WidePopを使用すること
* 
* @example
* var menu=[
* 	["a",function(){Alert("a");}],	//関数を実行する
* 	[],
* 	["b",[
* 		["b-a",function(){return ["b","a"];}],	//配列を返す
* 		["b-c","b-c"]	//文字列を返す
* 	]]
* ]
* var p=new SimplePM();
* p.create(menu);
* p.track();
*/
function SimplePM(menu){
	/**
	* メニュー一つ当たりの最大の項目数。
	* @const
	* @type {number}
	*/
	var maxMenuCount=100;
	var popup=null;
	var command={};
	var definition="";
	/**
	* ID→ツリー内の位置。
	*/
	var hash={};
	/**
	* ツリー内の位置→ID。
	*/
	var invertedHash={};
	/**
	* メニューの結果を表す。
	* @typedef {object} SimplePM~menuResult
	* @property {int} [id=0] メニューのID。キャンセルした場合は0
	* @property {*} [result=null] メニューのコマンドの実行結果。もしくはコマンドの内容もしくはメニューのテキスト
	*/
	/**
	* メニューの前回の実行結果。
	* @type {SimplePM~menuResult}
	*/
	var last={id:0,result:null};
	var pat=/^\d+(?:-\d+)$/;
	
	/**
	* ポップアップを表示、コマンドを実行する。
	* @param {number} [pos=0] 0ならキャレット位置に表示、1ならカーソル位置に表示する
	* @return {SimplePM~menuResult}
	*/
	this.track=function(pos){
		var id=popup.Track(pos);
		if(id&&id in command){
			last=new MenuResult(id,(command[id] instanceof Function?command[id]():command[id])||null);
		}
		else{
			last={id:0,result:null};
		}
		return last;
	}
	/**
	* サブメニューを作成する。
	* @method
	* @param {Array.<SimplePM~menuDefinition>} menu メニュー定義の配列
	* @param {number} [baseId] 最初の項目のID。デフォルトでは親メニューID×@maxMenuCount
	* @return {PopupMenu}
	*/
	var createSubMenu=(function(){
		var hashValue="";
		return function(menu,baseId){
			baseId=baseId||maxMenuCount;
			var sub=CreatePopupMenu();
			for(var i=0;i<menu.length&&i<maxMenuCount;i++){
				var id=baseId+i;
				if(!menu[i].length){
					sub.Add("",id,meMenuSeparator);
				}
				else if(!menu[i][0]&&typeof(menu[i][1])=="string"){
					sub.Add(menu[i][1],id,meMenuGrayed);
				}
				else if(menu[i][0]&&menu[i][1] instanceof Array){
					hashValue+="["+i+"]";
					hash[id]=hashValue.split("][").join("][1][");
					invertedHash[hash[id]]=id;
					sub.AddPopup(menu[i][0],arguments.callee(menu[i][1],id*maxMenuCount));
					hashValue="";
				}
				else{
					sub.Add(menu[i][0],id);
					command[id]=menu[i][1]||menu[i][0];
					hash[id]=(hashValue+"["+i+"]").split("][").join("][1][");
					invertedHash[hash[id]]=id;
				}
			}
			return sub;
		}
	})();
	/**
	* コマンドまたはサブメニュー。
	* @typedef {function():*|Array.<WidePM~menuDefinition>} SimplePM~commandOrChildren
	*/
	/**
	/**
	* メニュー項目の定義。<br />
	* 空にした場合区切りを作成する。
	* @typedef {Array} SimplePM~menuDefinition
	* @property {string} [0] メニューのテキスト
	* @property {SimplePM~commandOrChildren} [1] 関数であればメニュー選択時に実行する。
	* 	{@link SimplePM~menuDefinition}の配列であれば、サブメニューを作成する
	*/
	/**
	* メニューを作成する。
	* @param {Array.<SimplePM~menuDefinition>} menu メニューの定義。
	* @return {SimplePM}
	*/
	this.create=function(menu){
		hash={};
		invertedHash={};
		definition=menu;
		popup=createSubMenu(menu);
		return this;
	}
	/**
	* PopupMenuを取得する。
	* @return {PopupMenu}
	*/
	this.getPopup=function(){
		return popup;
	}
	/**
	* メニューの位置からIDを取得する。
	* @param {string} pos 位置。0起点で、上位の階層とは「-」で繋ぐ。区切りも数える
	* @return {number|undefined}
	* 
	* @example
	* //return "b-c"
	* p.getText(p.getId("2-1"));
	*/
	this.getId=function(pos){
		pos="["+pos.replace(/-/g,"][1][")+"]";
		return pos in invertedHash?invertedHash[pos]:undefined;
	}
	/**
	* 親メニューのIDを取得する。
	* @param {number|string} id IDもしくはメニューでの位置
	* @return {number}
	*/
	this.getParentId=function(id){
		var t=0;
		if(pat.test(id)){
			id=this.getId(id);
		}
		if(id in hash){
			t=hash[id].replace(/\[1\]\[[^\[\]]+\]$/,"");
			if(t){
				t=invertedHash[t];
			}
		}
		return t;
	}
	/**
	* 項目のテキストを取得する。
	* @param {number|string} id IDもしくはメニューでの位置
	* @return {string}
	*/
	this.getText=function(id){
		var t="";
		if(pat.test(id)){
			id=this.getId(id);
		}
		if(id in hash){
			t=eval("definition"+hash[id]);
			if(t){
				t=t[0];
			}
		}
		return t;
	}
	/**
	* IDの項目のコマンドを取得する。
	* @param {number|string} id IDもしくはメニューでの位置
	* @return {*} 未定義の場合はテキストを返す
	*/
	this.getCommand=function(id){
		if(id.test(/\d+(?:-\d+)/)){
			id=this.getId(id);
		}
		return command[id];
	}
	/**
	* 前回の実行結果を返す。
	* @return {SimplePM~menuResult}
	*/
	this.getResult=function(){
		return last;
	}

	if(menu){
		return this.create(menu);
	}
}
