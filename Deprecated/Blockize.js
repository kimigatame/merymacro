#title="ブロック挿入"
#include "kili/KILI.js"
#include "kili/StringEx.js"
#include "kili/WidePop.js"
#include "kili/json2.js"

/**
* 選択部分をテンプレートにはめ込んだブロックを挿入する。インデントも考慮する。<br />
* テンプレートの「<##>」が選択文字列に置き換えられる。{@link Blockize~here}で変更可能。<br />
* テンプレートに「<!!>」が有れば、補完後その位置にキャレットを移動する。{@link Blockize~caret}で変更可能。
* リストファイルはJSONで、形式は「"entries":[Blockize.Entry...]」。サンプルファイルを参照。
* @class
* @param {string} path リストファイルのパス
*/
function Blockize(path){
	/**
	* リストファイルのパス。
	* @type {string}
	*/
	var path;
	/**
	* 補完時の選択文字列を挿入する位置のアンカー。
	* @const
	* @type {string}
	*/
	var here="<##>";
	/**
	* 補完後にキャレットを移動する位置のアンカー。
	* @const
	* @type {string}
	*/
	var caret="<!!>";
	/**
	* ブロック挿入を実行する。
	*/
	this.execute=function(){
		var key="";
		var menu=[];
		var t=list.select(function(index,value){
			return value.template==undefined||value.modes.length==0||value.modes==GDO().Mode;
		});
		for(var i=0;i<t.length;i++){
			if(t[i].template==undefined){
				if(menu.length&&menu.last().length>0&&i<t.length-1){
					menu.push([]);
				}
			}
			else{
				menu.push([t[i].name,1,t[i].template]);
			}
		}
		if(!menu.length){
			menu.push(["候補がありません",32]);
		}
		else{
		menu.push([],["確認モード",2]);
		}
		var popup=new WidePop(menu);
		var result=popup.create().track(0,true);
		while(result.value){
			var seg=result.value.unescape();
			if(!popup.getCheckBox()[0].checked||Confirm("補完しますか?\n\n"+seg)){
				applyText(seg);
				break;
			}
			result=popup.track(0);
		}
	};
	/**
	* テンプレートと選択文字列からブロックを作成する。
	* @method
	* @param {string} template 
	*/
	var applyText=function(template){
		var $s=GSO();
		template=template.crlf().split("\r\n");
		KILI.selex.excludeBreak();
		var str=$s.Text.split("\n");
		//TopPointが行頭なら選択範囲のインデントを利用する
		var head=KILI.selex.getTopPoint().x==1;
		var left=head?str[0]:KILI.selex.getLeft();
		var breakLeft=!(head?/^\s*/.test(left):/^\s*$/.test(left));
		var indent=left.match(/^\s*/)[0];
		var breakRight=!/^\s*$/.test(KILI.selex.getRight());
		var indent2="";
		var distance=0;
		if(head){
			template[0]=indent+template[0];
			str[0]=str[0].substr(indent.length);
		}
		if(breakLeft){
			template[0]="\n"+indent+template[0];
		}
		if(breakRight){
			template[template.length-1]+="\n"+indent;
		}
		for(var i=1;i<template.length;i++){
			//テンプレートにあるインデントを取得
			if(template[i].indexOf(here)>0){
				indent2=/^\s+/.test(template[i])?RegExp.lastMatch:"";
			}
			template[i]=indent+template[i];
		}
		for(var i=1;i<str.length;i++){
			str[i]=indent2+str[i];
		}
		template=template.join("\n").replace(here,str.join("\n"));
		template=template.replace(caret,function(){
			distance+=arguments[1];
			return "";
		});
		$s.Text=template.crlf();
		if(distance>0){
			$s.CharLeft(false,template.length-distance);
		}
	}
	
	var file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(path,1,true,-1);
	if(!file.AtEndOfStream){
		list=JSON.parse(file.ReadAll())["entries"];
	}
	file.Close();
}
Blockize.Entry=function(){
	/**
	* 項目名。
	* @type {string}
	*/
	this.name;
	/**
	* 検索パターン。
	* @type {string}
	*/
	this.template;
	/**
	* 対応する編集モード。空の配列ならば全ての編集モードに対応する
	* @type {Array.<string>}
	*/
	this.modes;
}

//このファイルを直接実行する場合は次行のコメントを解除する。
//new Blockize(ScriptFullName.split("\\").slice(0,-1).join("\\")+"\\blockize.json").execute();
