#title="賢く改行"
#include "kili/KILI.js"
#include "kili/StringEx.js"
#include "kili/ArrayEx.js"
#include "kili/json2.js"

/**
* ちょっと賢く改行する。
* <ul>
* <li>ブロック開始記号の直後で改行した場合にブロック（次行インデント+閉じ括弧）を作成する。選択範囲直後がブロック終了記号だった場合はそれを利用する。
* <ul>
* <li>ブロックを作る場合、選択部分は次行へ。</li>
* <li>ブロックを作らない場合、選択部分は削除。</li>
* <li>選択範囲以降の文字列の位置は指定する。</li>
* </ul>
* </li>
* <li>行コメント内の場合、次行も行コメントにする。</li>
* </ul>
* @param {string} path 編集モード設定ファイルのパス
* @param {?string} [mode="DEFAULT"] 編集モード
* @param {?number} [exception=0] 改行時に「追加しない」もの
* <table>
* <tr><td>1</td><td>行コメント</td></tr>
* <tr><td>2</td><td>ブロック</td></tr>
* <tr><td>4</td><td>行コメント中のブロック</td></tr>
* <tr><td>8</td><td>元々のインデント</td></tr>
* <tr><td>16</td><td>ブロック内のインデント</td></tr>
* </table>
* @param {?number} [push=0] 選択範囲以降の文字列を、-1ならそのまま、0なら次行に追加、1なら最後に追加、2なら削除する
* @class
*/
var SmartBreak=function(path,mode,exception,push){
	var $d=GDO();
	var $s=$d.Selection;
	if($s.GetActivePointX(mePosLogical)==1){
		$s.Text="\n";
		return;
	}
	/**
	* 編集モードの設定。
	* @typedef {object} SmartBreak~mode
	* @property {Array.<string>} [comment] 行コメント文字列
	* @property {object.<string,string>} [bracket] ブロックの開始と終了の文字列のペア
	* @property {string} [indent="\t"] インデントに使う文字列
	*/
	/**
	* 編集モードの設定のリスト。<br />
	* モード名は大文字にすること。
	* @typedef {object.<string,mode>} SmartBreak~modes
	*/
	var modes;
	var file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(path,1,true,-1);
	if(!file.AtEndOfStream){
		modes=JSON.parse(file.ReadAll());
	}
	file.Close();
	mode=(mode||$d.Mode).toUpperCase();
	mode=mode in modes?modes[mode]:modes.DEFAULT;
	var comment=mode.comment||[];
	var bracket=mode.bracket||{};
	var left=KILI.selex.getLeft();
	var right=KILI.selex.getRight();
	var sel=$s.Text;
	var originalIndent=/^\s+/.test(left)?RegExp.lastMatch:"";
	for(var i=0;i<comment.length;i++){
		comment[i]=comment[i].escapeRegExp();
	}
	var lineComment=left.startsWith(new RegExp(originalIndent+comment.join("|").enclose("(",")")));
	comment=RegExp.$1;
	var open=[];
	for(var c in bracket){
		open.push(c.escapeRegExp());
	}
	var blockStart=open.length>0&&left.endsWith(new RegExp(open.join("|").enclose("(?:",")")));
	var close=blockStart?bracket[RegExp.lastMatch]:"";
	var blockEnd=blockStart&&right.startsWith(new RegExp(close.escapeRegExp()));
	var seg=[""];//[0]はキャレット行になる
	var addComment=!(exception&1);
	var addBlock=!(exception&2);
	var addBlockInComment=addComment&&addBlock&&!(exception&4);//行コメント中のブロックは、行コメントとブロックを作成する場合のみ有効
	var addOriginalIndent=!(exception&8);
	var indent=exception&16?"":mode.indent||"\t";
	var delta=1;
	if(addBlock&&blockStart&&(addBlockInComment||!lineComment)){
		seg.push(indent+sel);
		delta+=indent.length;
		if(blockEnd){//選択範囲直後がブロック終了記号ならそれを利用する
			right=right.slice(close.length);
		}
		if(push==0&&right.length){
			if(sel==""){//無選択の場合、ブロックの中身は選択範囲の右のみにする
				seg[seg.length-1]=indent+right;
			}
			else{
				seg.push(indent+right);
			}
		}
		else if(push==-1){
			seg[0]=right;
			delta+=right.length;
		}
		seg.push(close+(push==1?right:""));
	}
	else{//ブロックを作らない場合、選択範囲は削除
		seg.push(right);
	}
	if(addComment&&lineComment){
		for(var i=1;i<seg.length;i++){
			seg[i]=comment+seg[i];
		}
		delta+=comment.length;
	}
	if(addOriginalIndent){
		for(var i=1;i<seg.length;i++){
			seg[i]=originalIndent+seg[i];
		}
		delta+=originalIndent.length;
	}
	var top=KILI.selex.getTopPos();
	$s.SetActivePos(top);
	$s.EndOfLine(true,mePosLogical);
	$s.Text=seg.join("\n");
	$s.SetActivePos(delta+top);
}
//このファイルを直接実行する場合は次行のコメントを解除する。
//SmartBreak(ScriptFullName.split("\\").slice(0,-1).join("\\")+"\\smartbreak.json",null,null,1);
