#title="単純JSON読み書き"
#include "json2.js"

/**
* 共通のキーを持つオブジェクトを配列に纏めたDictionary。<br />
* 複数のオブジェクトをキーに基づいて配列に纏め、キーと配列とでDictionaryを構成する。<br />
* DictionaryはJSONとして読み書きする。
* @class
* @param {string} path リストファイルのパス
* @param {boolean} [force=false] キーが同じ要素が1つでも強制的に配列にする
* @param {Array.<string>} labels {@link KeyList#register}で使用する、要素の各項目を表すラベル
* 
* @deprecated
*/
function KeyList(path,force,labels){
	/**
	* リスト本体。
	* @type {Object.<string,Array.<*>>}
	*/
	this.list={};
	/**
	* リストファイルのパス。
	* @type {string}
	*/
	this.path=path;
	/**
	* 要素の各項目を表すラベル。{@link KeyList#register}で使用される。
	* @type {Array.<string>}
	*/
	this.labels=labels||[];
	/**
	* ダイアログの入力欄のデフォルトテキスト。{@link KeyList#register}で使用される。
	* @type {string}
	*/
	this.text="";
	/**
	* キーが同じ要素が1つでも強制的に配列にする。
	* @type {boolean}
	*/
	this.force=force||false;
}
/**
* リストを読み込む。
* @param {string} [key] 指定したキーのみ返す
* @return {object} リスト
*/
KeyList.prototype.read=function(key){
	var file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(this.path,1,true,-1);
	if(!file.AtEndOfStream){
		this.list=JSON.parse(file.ReadAll());
	}
	file.Close();
	return key in this.list?this.list[key]:this.list;
};
/**
* ファイルに書き込む。
*/
KeyList.prototype.write=function(){
	var file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(this.path,2,true,-1);
	file.Write(JSON.stringify(this.list,null,"\t"));
	file.Close();
}
/**
* ダイアログを使って要素を登録する。
* @param {number} count 一要素の項目数（キー含む）。この数-1回だけダイアログを表示する
* @param {string} key キー
* @param {...*} [args] 要素の項目の内容。指定されている場合、ダイアログでの入力をスキップする
*/
KeyList.prototype.register=function(count,key,args){
	var seg="を入力";
	var t=[];
	for(var i=0;i<count;i++){
		if(arguments[i+1]){
			t[i]=arguments[i+1].toString().escape();
		}
		else{
			t[i]=Prompt((this.labels[i]||"項目"+(i+1))+seg,this.text);
			if(!t[i]){
				return;
			}
		}
	}
	this.add.apply(this,t);
};
/**
* リストに項目を登録する。
* @param {string} key キー
* @param {...*} [args] 要素の各項目
*/
KeyList.prototype.add=function(key,args){
	if(arguments.length==0){
		return;
	}
	if(!(key in this.list)){
		this.list[key]=[];
	}
	this.list[key].push(!this.force&&arguments.length==2?arguments[1]:Array.prototype.slice.call(arguments,1));
};
