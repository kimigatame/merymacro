#title="スニペット"
#include "kili/KILI.js"
#include "kili/StringEx.js"
#include "kili/WidePop.js"
#include "kili/json2.js"

/**
* キャレット直前の文字列をキーとして補完する。インデントも考慮する。<br />
* リストファイルはJSONで、形式は「"entries":[SSnippet.Entry...]」。サンプルファイルを参照。<br />
* 以下の様に動作する。
* <ul>
* <li>同じキーの候補が複数ある場合はポップアップから選択する</li>
* <li>「確認モード」で補完前に内容を確認できる</li>
* <li>選択している場合はそれをキーとし、候補が1つでもポップアップを表示する</li>
* <li>補完内容に「<!!>」が有れば、補完後その位置にキャレットを移動する（{@link SSnippet~caret}で変更可能）</li>
* </ul>
* @class
* @extends KeyList
* @param {string} path リストファイル
*/
function SSnippet(path){
	/**
	* リストファイルのパス。
	* @type {string}
	*/
	var path;
	/**
	* 補完後にキャレットを移動する位置のアンカー。
	* @type {string}
	*/
	var caret="<!!>";
	/**
	* 補完を実行する。
	* @param {string} pattern キー検索用パターン
	* @param {boolean} [indierct] 候補が1つでも必ずポップアップを表示する
	*/
	this.execute=function(pattern,indirect){
		var $s=GSO();
		var left=KILI.selex.getLeft(true);
		var key;
		if($s.IsEmpty){
			key=new RegExp((pattern||"\\w+").enclose("(?:",")")+"$").test(left)?RegExp.lastMatch:"";
			if(key){
				$s.SetAnchorPoint(mePosLogical,RegExp.index+1,$s.GetActivePointY(mePosLogical));
			}
		}
		else{
			key=$s.Text;
			indirect=true;
		}
		if(!key){
			Status="キーが見つかりません。";
			return;
		}
		this.text=$s.Text;
		var menu=(list[key]||[]).select(function(index,value){
			return value.template==undefined||value.modes.length==0||value.modes==GDO().Mode;
		});;
		indirect=indirect||menu.length!=1;
		for(var i=0;i<menu.length;i++){
			menu[i]=[menu[i].name,1,menu[i].template];
		}
		if(!menu.length){
			menu.push(["候補がありません",32]);
		}
		var indent=/^\s+/.test(left)?RegExp.lastMatch:"";
		var distance=0;
		var applyText=function(seg){
			for(var i=1;i<seg.length;i++){
				seg[i]=indent+seg[i];
			}
			seg=seg.join("\n").replace(caret,function(){
				distance=arguments[1];
				return "";
			});
			$s.Text=seg.crlf();
			if(distance>0){
				$s.CharLeft(false,seg.length-distance);
			}
		}
		if(indirect){
			var popup=new WidePop(menu);
			var result=popup.create().track(0,true);
			while(result.result){
				var seg=result.result.unescape();
				if(!popup.isChecked(popup.getId(menu.length-1))||Confirm("補完しますか?\n\n"+seg)){
					applyText(seg.crlf().split("\r\n"));
					break;
				}
				result=popup.track(0);
			}
		}
		else{
			applyText(menu[0][2].unescape().split("\n"));
		}
	};
	
	var file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(path,1,true,-1);
	if(!file.AtEndOfStream){
		list=JSON.parse(file.ReadAll());
	}
	file.Close();
}
/**
* 項目。
*/
SSnippet.Entry=function(){
	/**
	* 項目名。
	* @type {string}
	*/
	this.name;
	/**
	* 検索パターン。
	* @type {string}
	*/
	this.template;
	/**
	* 対応する編集モード。空の配列ならば全ての編集モードに対応する
	* @type {Array.<string>}
	*/
	this.modes;
}

//このファイルを直接実行する場合は次行のコメントを解除する。
//new SSnippet(SSnippet.file=ScriptFullName.split("\\").slice(0,-1).join("\\")+"\\ssnippet.json").execute("\\.?[\\w]+|[ +]");
