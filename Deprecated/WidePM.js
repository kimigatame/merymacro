#title="拡張ポップアップメニュー"
#include "ArrayEx.js"
#include "MouseEmulator.js"

/**
* 実行コマンドを指定できるPopupMenu。{@link SimplePM}との違いは以下の通り。
*<ul>
* <li>IDが指定できる</li>
* <li>チェック状態、有効･無効を変更できる</li>
* <li>チェックボックスやラジオボタンとして使える</li>
* <li>項目を差し替えられる</li>
* <li>直接コマンドを実行できる</li>
* </ul>
* @class
* @param {WidePM~menuDefinition} menu メニューの定義
* @param {boolean} autoId IDを自動的に割り振るかどうか
* 
* @deprecated WidePopを使用すること
* 
* @example
* var menu=[
* 	["a",1,1,function(arg){Alert(arg?arg.isChecked(3):"arguments is null");}],
* 	[],
* 	["b",9,2],	//チェック&グループ開始
* 	["c",9,3],	//"b"がチェック済みなのでチェックされない
* 	["d",8,4],	//次のflagが0なのでここでグループ終了
* 	["e",0,5,[
* 		["e-a",0,6,"e-a"],	//チェックボックス
* 		["e-b",2,7]	//無効になっている
* 	]]
* ];
* var p=new WidePM(menu);
* p.track(0,true);
*/
function WidePM(menu,autoId){
	/**
	* メニュー一つ当たりの最大の項目数。
	* @const
	* @type {number}
	*/
	var maxMenuCount=100;
	var popup=null;
	var definition=[];
	var command={};
	/**
	* ID→ツリー内の位置。
	*/
	var hash={};
	/**
	* ツリー内の位置→ID。
	*/
	var invertedHash={};
	var checkbox={};
	var radioGroup=[];
	/**
	* ID→radioGroupのIndex
	*/
	var radiobutton={};
	/**
	* メニューの結果を表す。
	* @typedef {object} WidePM~menuResult
	* @property {int} [id=0] メニューのID。キャンセルした場合は0
	* @property {*} [result=null] メニューのコマンドの実行結果。もしくはコマンドの内容もしくはメニューのテキスト
	*/
	/**
	* メニューの前回の実行結果。
	* @type {WidePM~menuResult}
	*/
	var last={id:0,result:null};
	var auto=false;
	var pat=/^\d+(?:-\d+)*$/;
	
	/**
	* ポップアップを表示、コマンドを実行する。
	* @param {number} [pos=0] 0ならキャレット位置に表示、1ならカーソル位置に表示する
	* @param {boolean} [sendArg=false] コマンドに引数としてインスタンスを送るかどうか
	* @param {boolean} mouse {@link MouseEmurator}を使ってポップアップの位置を固定するかどうか
	* @return {WidePM~menuResult}
	*/
	this.track=function(pos,sendArg,mouse){
		last.result=null;
		if(mouse){
			var me=new MouseEmulator();
			var p=me.getXY();
		}
		while(last.id=popup.Track(pos)){
			if(last.id in command){
				last=this.execute(last.id,sendArg);
				break;
			}
			else{
				this.check(last.id,-1,true);
			}
			if(mouse&&pos){
				me.setXY(p.x,p.y);
			}
		}
		return last;
	}
	/**
	* サブメニューを作成する。
	* @method
	* @param {Array} menu メニュー定義の配列
	* @param {number} [baseId] 最初の項目のID。指定しなければ親メニューID×{@link WidePM~maxMenuCount}
	* @return {PopupMenu}
	*/
	var createSubMenu=(function(){
		var hashValue="";
		var radioCreating=false;
		var radioChecked=false;
		return function(menu,baseId){
			baseId=baseId||maxMenuCount;
			var sub=CreatePopupMenu();
			for(var i=0;i<menu.length&&i<maxMenuCount;i++){
				var id=auto?baseId+i:menu[i][2];
				if(!menu[i].length){
					sub.Add("",0,meMenuSeparator);
					radioCreating=radioChecked=false;
				}
				else if(menu[i][auto^3] instanceof Array){
					hashValue+="["+i+"]";
					hash[id]=hashValue.split("][").join("]["+(auto^3)+"][");
					invertedHash[hash[id]]=id;
					radioCreating=radioChecked=false;
					if(menu[i][1]&2){
						sub.Add(menu[i][0],id,menu[i][1]&3);
					}
					else{
						sub.AddPopup(menu[i][0],arguments.callee(menu[i][auto^3],id*maxMenuCount));
					}
					hashValue="";
				}
				else{
					if(menu[i][auto^3]){
						command[id]=menu[i][auto^3];
						radioCreating=radioChecked=false;
					}
					else{
						checkbox[id]=!!(menu[i][1]&1);
						if(radioCreating){
							if(!(menu[i][1]&8)){
								radioCreating=radioChecked=false;
							}
							else if(radioChecked){
								menu[i][1]&=10;
								radioGroup.last()[id]=false;
							}
							else{
								radioChecked=radioGroup.last()[id]=menu[i][1]&1;
							}
						}
						else if(menu[i][1]&8){
							radioCreating=true;
							radioGroup.push({});
							radioChecked=radioGroup.last()[id]=menu[i][1]&1;
						}
					}
					sub.Add(menu[i][0],id,menu[i][1]&3);
					hash[id]=(hashValue+"["+i+"]").split("][").join("]["+(auto^3)+"][");
					invertedHash[hash[id]]=id;
				}
			}
			return sub;
		}
	})();
	/**
	* コマンドまたはサブメニュー。
	* @typedef {function():*|Array.<WidePM~menuDefinition>} WidePM~commandOrChildren
	*/
	/**
	* メニュー項目の定義。<br />
	* 空にした場合区切りを作成する。<br /><br />
	* {@link WidePM~commandOrChildren}がnullの場合、その項目はチェックボックスもしくはラジオボタンとなる。<br />
	* 	なので何もしない項目の{@link WidePM~commandOrChildren}には適当な数字や文字列を設定しておけばいい。<br />
	* 	隣接するラジオボタン同士はグループとなり、ラジオボタンでない項目が来るとそこで終了する。
	* @typedef {Array} WidePM~menuDefinition
	* @property {string} [0] メニューのテキスト
	* @property {int} [1] ボタンの状態を表すフラグ
	* <table>
	* <tr><td>1</td><td>チェックする</td></tr>
	* <tr><td>2</td><td>無効にする</td></tr>
	* <tr><td>8</td><td>ラジオボタンにする</td></tr>
	* </table>
	* @property {int|WidePM~commandOrChildren} [2] ID。ID自動割り振りの場合は{@link WidePM~commandOrChildren}をとる
	* @property {WidePM~commandOrChildren} [3] 関数であればメニュー選択時に実行する。
	* 	{@link WidePM~menuDefinition}の配列であれば、サブメニューを作成する
	*/
	/**
	* メニューを作成する。
	* @param {Array.<WidePM~menuDefinition>} menu メニュー定義の配列
	* @param {boolean} [autoId=false] IDを自動的に割り振るかどうか
	* @return {WidePM}
	*/
	this.create=function(menu,autoId){
		command={};
		hash={};
		invertedHash={};
		checkbox={};
		radioGroup=[];
		radiobutton={};
		if(menu){
			definition=menu;
		}
		auto=autoId;
		popup=createSubMenu(definition);
		for(var i=0;i<radioGroup.length;i++){
			for(var c in radioGroup[i]){
				radiobutton[c]=i;
			}
		}
		return this;
	}
	/**
	* ポップアップメニューを取得する。
	* @return {PopupMenu}
	*/
	this.getPopup=function(){
		return popup;
	}
	/**
	* メニューの位置からIDを取得する。
	* @param {string} pos 位置。0起点で、上位の階層とは「-」で繋ぐ。区切りも数える
	* @return {number|undefined}
	* 
	* @example
	* //return "b-c"
	* p.getText(p.getId("2-1"));
	*/
	this.getId=function(pos){
		pos="["+pos.toString().replace(/-/g,"]["+(auto^3)+"][")+"]";
		return pos in invertedHash?invertedHash[pos]:undefined;
	}
	/**
	* 親メニューのIDを取得する。
	* @param {number|string} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @return {number}
	*/
	this.getParentId=function(id){
		var t=0;
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		if(id in hash){
			t=hash[id].replace(new RegExp("\\["+(auto^3)+"\\]"+"\\[[^\\[\\]]+\\]$"),"");
			if(t){
				t=invertedHash[t];
			}
		}
		return t;
	}
	/**
	* 項目のテキストを取得する。
	* @param {number|string} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @return {string}
	*/
	this.getText=function(id){
		var t="";
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		if(id in hash){
			t=eval("definition"+hash[id]);
			if(t){
				t=t[0];
			}
		}
		return t;
	}
	/**
	* IDで指定した項目のコマンドを返す。
	* @param {number|string} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @return {*}
	*/
	this.getCommand=function(id){
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		return command[id];
	}
	/**
	* 前回の実行結果を返す。
	* @return {WidePM~menuResult}
	*/
	this.getResult=function(){
		return last;
	}
	/**
	* チェックボックスとなっている項目とチェック状態を取得する。
	* @return {object.<id:boolean>}
	*/
	this.getCheckbox=function(){
		return checkbox;
	}
	/**
	* チェックされている項目のIDを取得する。
	* @return {Array.<int>}
	*/
	this.getChecked=function(){
		var result=[];
		for(var c in checkbox){
			if(checkbox[c]){
				result.push(c);
			}
		}
		return result;
	}
	/**
	* 項目の属するラジオボタングループを取得する。
	* @param {number|string} id IDもしくはメニューでの位置。
	* @see WidePM#getId
	* @return {object.<number,boolean>}
	*/
	this.getRadioGroup=function(id){
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		return radioGroup[radiobutton[id]]||null;
	}
	/**
	* メニューの一部を変更する。
	* @method
	* @param {number|string} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @param {Array.<WidePM~menuDefinition>} item 置き換えるメニューの定義
	* @param {?Function} condition この条件を満たしたときのみ変更する
	* @return {WidePM}
	* 
	* @example
	* p.modify(p.getId("5-1"),menu[0]).track();
	*/
	this.modify=function(id,item,condition){
		if(!(condition instanceof Function)||condition()){
			if(typeof(id)=="string"&&pat.test(id)){
				id=this.getId(id);
			}
			if(id in hash){
				eval("definition"+hash[id]+"=item");
				return this.create(definition,auto);
			}
		}
		return this;
	}
	/**
	* 項目のチェックを切り換える。<br />
	* ラジオボタンの場合は常にチェックのみ。
	* @param {number|string} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @param {number} [check=0]
	* <table>
	* <tr><td>-1</td><td>トグル</td></tr>
	* <tr><td>0</td><td>チェック</td></tr>
	* <tr><td>1</td><td>チェック解除</td></tr>
	* </table>
	* @method
	* @param {boolean} [create=false] 変更後PopupMenuを作成する
	* @param {?Function} condition この条件を満たしたときのみ変更する
	* @return {WidePM}
	*/
	this.check=(function(){
		var evaluate=function(id,check){
			if(id in radiobutton){
				var t=radioGroup[radiobutton[id]];
				for(var c in t){
					eval("definition"+hash[c]+"[1]"+(c==id?"|=1":"&=10"));
				}
			}
			else if(id in hash){
				eval("definition"+hash[id]+"[1]"+(check?check<0?"^=1":"&=10":"|=1"));
			}
		}
		return function(id,check,create,condition){
			if(!(condition instanceof Function)||condition()){
				if(id instanceof Array){
					for(var i=0;i<id.length;i++){
					if(typeof(id[i])=="string"&&pat.test(id[i])){
						id[i]=this.getId(id[i]);
					}
						evaluate(id[i],check);
					}
				}
				else{
					if(typeof(id)=="string"&&pat.test(id)){
						id=this.getId(id);
					}
					if(id in hash){
						evaluate(id,check);
					}
				}
			}
			return create?this.create(definition,auto):this;
		}
	})();
	/**
	* 項目の有効・無効を切り換える。
	* @param {number|string|Array} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @param {number} [available=0]
	* <table>
	* <tr><td>-1</td><td>トグル</td></tr>
	* <tr><td>0</td><td>有効</td></tr>
	* <tr><td>1</td><td>無効</td></tr>
	* </table>
	* @method
	* @param {boolean} [create=false] 変更後PopupMenuを作成する
	* @param {?Function} condition この条件を満たしたときのみ変更する
	* @return {WidePM}
	*/
	this.available=(function(){
		var evaluate=function(id,available){
			eval("definition"+hash[id]+"[1]"+(available?available<0?"^=2":"|=2":"&=9"));
		}
		return function(id,available,create,condition){
			if(!(condition instanceof Function)||condition()){
				if(id instanceof Array){
					for(var i=0;i<id.length;i++){
						if(typeof(id[i])=="string"&&pat.test(id[i])){
							id[i]=this.getId(id[i]);
						}
						if(id[i] in hash){
							evaluate(id[i],available);
						}
					}
				}
				else{
					if(typeof(id)=="string"&&pat.test(id)){
						id=this.getId(id);
					}
					if(id in hash){
						evaluate(id,available);
					}
				}
			}
			return create?this.create(definition,auto):this;
		}
	})();
	/**
	* IDの項目がチェックされているかどうかを取得する。
	* @param {number|string} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @return {boolean}
	*/
	this.isChecked=function(id){
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		return checkbox[id]||false;
	}
	/**
	* IDの項目が有効かどうかを取得する。
	* @param {number|string} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @return {boolean}
	*/
	this.isAvailable=function(id){
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		return !eval("definition"+hash[id]+"[1]&2");
	}
	/**
	* IDの項目がチェックボックスかどうかを取得する。
	* @param {number|string} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @return {boolean}
	*/
	this.isCheckBox=function(id){
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		return id in checkbox&&!(id in radiobutton);
	}
	/**
	* IDの項目がラジオボタンかどうかを取得する。
	* @param {number} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @return {boolean}
	*/
	this.isRadioButton=function(id){
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		return id in checkbox;
	}
	/**
	* コマンドを実行する。
	* @param {number} id IDもしくはメニューでの位置
	* @see WidePM#getId
	* @param {boolean} [sendArg=false] このオブジェクト自身をコマンドの引数にする
	* @return {Object} trackを参照。
	*/
	this.execute=function(id,sendArg){
		if(typeof(id)=="string"&&pat.test(id)){
			id=this.getId(id);
		}
		last.id=id;
		last.result=(command[id] instanceof Function?command[id](sendArg?this:null):command[id])||null;
		return last;
	}

	if(menu){
		return this.create(menu,autoId);
	}
}
