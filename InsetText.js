#title="テンプレート挿入"
#include "kili/Include.js"
#include "kili/json2.js"

/**
* キャレット行の検索結果に基づいてテンプレートから文字列を作成・挿入する。<br />
* <br />
* リストファイルはJSONで、形式はInsetText.Settings。サンプルファイルを参照。
* <br />
* 使用の流れは以下の通り。
* <ol>
* <li>InsetText#matchを実行することで、</li>
* <li>編集モードがInsetText.Entry#modesにマッチするエントリーを選択し、</li>
* <li>それぞれInsetText.Entry#conditionsに基づいて検索し、</li>
* <li>全てマッチした場合InsetText.Generatedを作成し、</li>
* <li>InsetText#generatedに格納する。</li>
* </ol>
* マッチするものが無かった場合はInsetText.Settings#subがあればInsetText#generatedに格納する。<br />
* それも無い場合は空の配列になる。
* <ol>
* <li>InsetText#generatedから必要なものを選択し、</li>
* <li>InsetText.Generated#insertを実行すると、</li>
* <li>InsetText.Entry#rangeの範囲に、</li>
* <li>InsetText.Entry#indentを適用した上で、</li>
* <li>InsetText.Generated#textを挿入する。</li>
* </ol>
* InsetText.Generatedには他にもプロパティがあるが、検索結果の保存用なので直接使用する必要はない。
* @class
* @param {string} path リストファイルのパス
*/
function InsetText(path){
	/**
	* リスト本体。
	* @type {Array.<InsetText.Entry>}
	*/
	var list=[];
	/**
	* 代替テンプレート。
	* @type {Array.<InsetText.Entry>}
	*/
	var sub=undefined;
	/**
	* 生成されたテキスト。
	* @type {Array.<InsetText.Generated>}
	*/
	this.generated=[];
	/**
	* キャレット行の情報。
	* @type {InsetText.Line}
	*/
	this.line=undefined;
	
	/**
	* 検索してテキストを生成する。<br />
	* @param {bool} [shortcut=false] 一つのエントリーがマッチした時点で終了する
	* @return {InsetText} 自身
	*/
	this.match=function(shortcut){
		var line=new InsetText.Line();
		var t=KILI.selex.getLeft();
		line.text=t;
		line.basePos=KILI.selex.getTopPos()-t.length;
		line.noblank.start=/(?=[^\s])/.test(t)?RegExp.leftContext.length:0;
		line.selection.start=line.text.length;
		t=GSO().Text;
		line.text+=t;
		line.selection.end=line.text.length;
		line.text+=KILI.selex.getRight();
		line.full.end=line.text.length;
		line.noblank.end=/[^\s](\s*)$/.test(line.text)?line.full.end-RegExp.$1.length:line.full.end;
		this.line=line;
		for(var i=0;i<list.length;i++){
			var matches=[];
			for(var j=0;j<list[i].conditions.length;j++){
				var text=list[i].conditions[j].text;
				var space=list[i].conditions[j].spaces;
				var required=list[i].conditions[j].type>0;
				var type=Math.abs(list[i].conditions[j].type);
				t="";
				switch(type){
					case InsetText.SearchTypes.head:
					case InsetText.SearchTypes.selectionFront:
					case InsetText.SearchTypes.beforeSelection:
					case InsetText.SearchTypes.beforeLength:
					case InsetText.SearchTypes.beforeContain:
					case InsetText.SearchTypes.beforeRegExp:
						t=line.text.substring(space?0:line.noblank.start,line.selection.start);
						break;
					case InsetText.SearchTypes.tail:
					case InsetText.SearchTypes.selectionBack:
					case InsetText.SearchTypes.afterSelection:
					case InsetText.SearchTypes.afterLength:
					case InsetText.SearchTypes.afterContain:
					case InsetText.SearchTypes.afterRegExp:
						t=line.text.substring(line.selection.end,space?line.full.end:line.noblank.end);
						break;
					case InsetText.SearchTypes.selection:
					case InsetText.SearchTypes.selectionLength:
					case InsetText.SearchTypes.selectionContain:
					case InsetText.SearchTypes.selectionRegExp:
						t=line.text.substring(line.selection.start,line.selection.end);
						break;
					case InsetText.SearchTypes.lineLength:
					case InsetText.SearchTypes.lineContain:
					case InsetText.SearchTypes.lineRegExp:
						t=space?line.text:line.text.substring(line.noblank.start,line.noblank.end);
						break;
					default:
						break;
				}
				switch(type){
					case InsetText.SearchTypes.head:
						if(t.startsWith(text)==required){
							t=space?0:line.noblank.start;
							matches.push(createMatch(required,t,t+text.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.tail:
						if(t.endsWith(text)==required){
							t=line.full.end-(space?0:line.full.end-line.noblank.end);
							matches.push(createMatch(required,t-text.length,t));
							continue;
						}
						break;
					case InsetText.SearchTypes.selectionFront:
						if(t.endsWith(text)==required){
							matches.push(createMatch(required,line.selection.start-text.length,line.selection.start));
							continue;
						}
						break;
					case InsetText.SearchTypes.selectionBack:
						if(t.startsWith(text)==required){
							matches.push(createMatch(required,line.selection.end,line.selection.end+text.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.beforeSelection:
						if((t==text)==required){
							matches.push(createMatch(required,line.selection.start-text.length,line.selection.start));
							continue;
						}
						break;
					case InsetText.SearchTypes.afterSelection:
						if((t==text)==required){
							matches.push(createMatch(required,line.selection.end,line.selection.end+text.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.selection:
						if((t==text)==required){
							matches.push(createMatch(required,line.selection.start,line.selection.end));
							continue;
						}
						break;
					case InsetText.SearchTypes.beforeLength:
						if((t.length==text)==required){
							matches.push(createMatch(required,line.selection.start-text.length,line.selection.start));
							continue;
						}
						break;
					case InsetText.SearchTypes.afterLength:
						if((t.length==text)==required){
							matches.push(createMatch(required,line.selection.end,line.selection.end+text.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.selectionLength:
						if((t.length==text)==required){
							matches.push(createMatch(required,line.selection.start,line.selection.end));
							continue;
						}
						break;
					case InsetText.SearchTypes.beforeContain:
						t=t.indexOf(text);
						if((t>-1)==required){
							t=space?t:t+line.noblank.start;
							matches.push(createMatch(required,t,t+text.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.afterContain:
						t=t.indexOf(text);
						if((t>-1)==required){
							t=line.selection.end+t;
							matches.push(createMatch(required,t,t+text.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.selectionContain:
						t=t.indexOf(text);
						if((t>-1)==required){
							t=line.selection.start+t;
							matches.push(createMatch(required,t,t+text.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.beforeRegExp:
						if(new RegExp(text).test(t)==required){
							t=space?RegExp.index:RegExp.index+line.noblank.start;
							matches.push(createMatch(required,t,t+RegExp.lastMatch.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.afterRegExp:
						if(new RegExp(text).test(t)==required){
							matches.push(createMatch(required,RegExp.index+line.selection.end,RegExp.lastIndex+line.selection.end));
							continue;
						}
						break;
					case InsetText.SearchTypes.selectionRegExp:
						if(new RegExp(text).test(t)==required){
							t=RegExp.lastIndex+line.selection.start;
							matches.push(createMatch(required,t,t+RegExp.lastMatch.length));
							continue;
						}
						break;
					case InsetText.SearchTypes.lineLength:
						if((t.length==text)==required){
							t=space?0:line.noblank.start;
							matches.push(createMatch(required,t,t+text.length));
						}
						break;
					case InsetText.SearchTypes.lineContain:
						t=t.indexOf(text);
						if((t>-1)==required){
							t=space?t:line.noblank.start+t;
							matches.push(createMatch(required,t,t+text.length));
						}
						break;
					case InsetText.SearchTypes.lineRegExp:
						if(new RegExp(text).test(t)==required){
							t=space?RegExp.index:RegExp.lastIndex+line.noblank.start;
							matches.push(createMatch(required,t,t+RegExp.lastMatch.length));
							continue;
						}
						break;
					default:
						break;
				}
			}
			if(matches.length!=list[i].conditions.length){
				continue;//マッチ失敗。このエントリーは終わり
			}
			this.generated.push(new InsetText.Generated(list[i],line,matches));
			if(shortcut){
				break;
			}
		}
		if(!this.generated.length&&sub){
			this.generated.push(new InsetText.Generated(sub,line,createMatch(false)));
		}
		else{
			this.generated=this.generated.sort(function(a,b){return (b.entry.weight||0)-(a.entry.weight||0);});
		}
		
		return this;
	}

	/**
	* マッチ範囲を表すオブジェクトを作成する。
	* @param {boolean} required 範囲を設定する必要がある
	* @param {number} [start] 開始位置
	* @param {number} [end] 終了位置
	* @return {InsetText.Range}
	*/
	function createMatch(required,start,end){
		if(required){
			return new InsetText.Range(start,end);
		}
		else{
			return new InsetText.Range();
		}
	}
	
	//constructor
	var file=new ActiveXObject("Scripting.FileSystemObject").OpenTextFile(path,1,true,-1);
	if(!file.AtEndOfStream){
		var mode=GDO().Mode;
		var t=JSON.parse(file.ReadAll());
		list=t.entries.select(function(index,value){return value.modes==null||value.modes.indexOf(mode)>-1;});
		sub=t.sub;
	}
	file.Close();
}
/**
* 範囲を表す文字列を解析する。
* @param {string} str 範囲を表す文字列
* @param {InsetText.Line} line 検索行
* @param {Array.<InsetText.Range>} matched マッチ結果
* @return {InsetText.Range}
*/
InsetText.parseRange=function(str,line,matches){
	var pattern=/^(\d+)$|^(?:(\d+):)?(?:(\d)([+-]\d+)?),(?:(\d+):)?(?:(\d)([+-]\d+)?)$/g;
	if(!str.match(pattern)){
		throw new Error(0,"range parse failed : "+str);
	}
	var cap1;
	var cap2;
	var pos1;
	var pos2;
	var delta1=0;
	var delta2=0;
	if(RegExp.$1){
		cap1=RegExp.$1;
		cap2=RegExp.$1;
		pos1=InsetText.RangeTypes.matchStart;
		pos2=InsetText.RangeTypes.matchEnd;
	}
	else{
		cap1=RegExp.$2||0;
		cap2=RegExp.$5||0;
		pos1=RegExp.$3;
		pos2=RegExp.$6;
		delta1=parseInt(RegExp.$4)||0;
		delta2=parseInt(RegExp.$7)||0;
	}
	var t=[parseInt(pos1),parseInt(pos2||InsetText.RangeTypes.matchEnd)];
	for(var i=0;i<t.length;i++){
		switch(t[i]){
			case InsetText.RangeTypes.noblankStart:
				t[i]=line.noblank.start;
				break;
			case InsetText.RangeTypes.noblankEnd:
				t[i]=line.noblank.end;
				break;
			case InsetText.RangeTypes.selectionStart:
				t[i]=line.selection.start;
				break;
			case InsetText.RangeTypes.selectionEnd:
				t[i]=line.selection.end;
				break;
			case InsetText.RangeTypes.fullStart:
				t[i]=line.full.start;
				break;
			case InsetText.RangeTypes.fullEnd:
				t[i]=line.full.end;
				break;
			case InsetText.RangeTypes.matchStart:
				t[i]=matches[i==1&&cap2?cap2:cap1||0].start;
				break;
			case InsetText.RangeTypes.matchEnd:
				t[i]=matches[i==1&&cap2?cap2:cap1|0].end;
				break;
			default:
				t[i]=i==0?matches[cap1||0].start:matches[cap2||cap1||0].end;
				break;
		}
	}
	return new InsetText.Range((t[0]+delta1).clamp(line.full.start,line.full.end),(t[1]+delta2).clamp(line.full.start,line.full.end));
}
/**
* キャレット行の情報。<br />
* basePosを除き、位置はInsetText.Line#textにおけるインデックス。
* @class
*/
InsetText.Line=function(){
	/**
	* 選択行全体。
	* @type {string}
	*/
	this.text=undefined;
	/**
	* 対象範囲の先頭からの位置。
	* @type {number}
	*/
	this.basePos=0;
	/**
	* 前後の空白を除いた位置。
	* @type {InsetText.Range}
	*/
	this.noblank=new InsetText.Range();
	/**
	* 選択部分の位置。
	* @type {InsetText.Range}
	*/
	this.selection=new InsetText.Range();
	/**
	* 先頭と末尾の位置。startは常に0、endは常にInsetText.Line#textの長さに等しい。
	* @type {InsetText.Range}
	*/
	this.full=new InsetText.Range();
}
/**
* 範囲を表す。
* @class
* @param {number} [start=0] 開始位置
* @param {number} [end=0] 終了位置
*/
InsetText.Range=function(start,end){
	/**
	* 開始位置。
	* @type {number}
	*/
	this.start=start||0;
	/**
	* 終了位置。
	* @type {number}
	*/
	this.end=end||0;
	/**
	* 配列として返す。
	* @type {number}
	* @return {Array.<number>}
	*/
	this.toArray=function(){
		return [this.start,this.end];
	}
	/**
	* 長さを返す。
	* @type {number}
	* @return {number}
	*/
	this.getLength=function(){
		return Math.abs(this.start-this.end);
	}
}
/**
* テンプレートと検索結果から生成されたテキストと関連情報。
* @class
* @param {InsetText.Entry} entry 対応するエントリー
* @param {InsetText.Line} line 検索行の情報
* @param {Array.<InsetText.Range>} matches マッチ結果
*/
InsetText.Generated=function(entry,line,matches){
	/**
	* 名前。
	* @type {string}
	*/
	this.name=entry.name;
	/**
	* 対応するエントリー。
	* @type {InsetText.Entry}
	*/
	this.entry=entry;
	/**
	* 生成されたテキスト。
	* @type {string}
	*/
	this.text=undefined;
	/**
	* 挿入後の選択位置。InsetText.Generated#line#full#startを基準とする。
	* @type {InsetText.Range}
	*/
	this.range=undefined;
	/**
	* 検索行の情報。
	* @type {InsetText.Line}
	*/
	this.line=line;
	/**
	* マッチ結果。否定検索の場合、全ての値は常に0。
	* @type {Array.<InsetText.Range>}
	*/
	this.matches=matches;
	/**
	* マッチ結果に基づいて範囲を選択する。
	*/
	this.setInsertRange=function(){
		var range=InsetText.parseRange(this.entry.range||"3,4",this.line,this.matches);
		var t=Math.min(range.start,range.end);
		this.range.start+=t;
		this.range.end+=t;
		KILI.selex.setRange(range.start+this.line.basePos,range.end+this.line.basePos);
	}
	/**
	* テキストを挿入する。<br />
	*/
	this.insert=function(){
		this.setInsertRange();
		GSO().Text=this.text;
		KILI.selex.setRange(this.range.start+this.line.basePos,this.range.end+this.line.basePos);
	}
	
	//constructor
	var pattern=/\{(\d+|(?:\d+:)?\d(?:[+-]\d+)?,(?:\d+:)?\d(?:[+-]\d+)?)\}/g;
	var t=entry.template.replace(pattern,function($1,$2){
		var range=InsetText.parseRange($2,line,matches);
		return line.text.substring(range.start,range.end);
	});
	t=t.split("{{}").join("{").split("{}}").join("}");
	if(entry.indent){
		t=t.split("\n").join("\n"+line.text.substring(line.full.start,line.noblank.start));
	}
	
	this.range=new InsetText.Range(t.indexOf("{s}"),t.indexOf("{e}"));
	this.text=t.split("{s}").join("").split("{e}").join("");
	if(this.range.start==-1&&this.range.end==-1){
		this.range.start=this.text.length;
		this.range.end=this.range.start;
	}
	else if(this.range.start==-1){
		this.range.start=this.range.end;
	}
	else if(this.range.end==-1){
		this.range.end=this.range.start;
	}
	if(this.range.start>this.range.end){
		this.range.start-=3;
	}
	else if(this.range.start!=this.range.end){
		this.range.end-=3;
	}
}
/**
* テンプレートと検索条件。
* @class
*/
InsetText.Settings=function(){
	/**
	* 全てのエントリーがマッチしなかった場合に使用される代替テンプレート。<br />
	* InsetText.Entry#conditionsは一切考慮されない。
	* @type {InsetText.Entry}
	*/
	this.sub=undefined;
	/**
	* テンプレートリスト。
	* @type {Array.<InsetText.Entry>}
	*/
	this.entries=undefined;
}
/**
* テンプレートと検索条件。
* @class
*/
InsetText.Entry=function(){
	/**
	* 名前。
	* @type {string}
	*/
	this.name=undefined;
	/**
	* テンプレート。<br />
	* InsetText.Entry#rangeと同じ文字列を「{}」で括って埋め込むと、その範囲の文字列で置換される。<br />
	* 「{s}」「{e}」はそれぞれ挿入後の選択開始位置、選択終了位置を表す。一方のみだとその位置にキャレットを移動する。
	* 置換パターンは「{{}」「{}}」でエスケープできる。
	* @type {string}
	*/
	this.template=undefined;
	/**
	* 複数行に渡る場合、インデントを1行目に揃える。
	* @type {boolean}
	*/
	this.indent=undefined;
	/**
	* 検索条件。
	* @type {Array.<InsetText.Condition>}
	*/
	this.conditions=undefined;
	/**
	* 挿入位置の範囲設定。<br />
	* 「index」または「[index:]startPos[delta],[index:]endPos[delta]」の形式で検索によって得られたInsetText.RangeとInsetText.Lineに基づいて範囲を指定する。<br />
	* indexはInsetText.Generated#matchesのインデックスを表す。<br />
	* startPosとendPosはInsetText.RangeTypesを指定する事で、その範囲の文字列に置換される。<br />
	* deltaはそれぞれの位置からの差分を表す。但しInsetText.Line#fullに挿入したテキストの長さを加えた範囲に丸められる。<br/>
	* indexのみ指定した場合は「index:7,index:8」と同じ。<br />
	* インデックスを省略した場合は「0」とみなす。<br />
	* nullと解される場合は「3,4」とみなす。
	* @type {string}
	*/
	this.range=undefined;
	/**
	* 適用する編集モード。指定しなければ全モードで使用される。
	* @type {Array.<string>}
	*/
	this.modes=undefined;
	/**
	* 優先度。大きい程InsetText#generatedの先頭に来る。
	* @type {number}
	*/
	this.weight=undefined;
}
/**
* 検索条件。
* @class
*/
InsetText.Condition=function(){
	/**
	* 検索タイプ。InsetText.SearchTypesを用いるが負の値にすると否定検索となる
	* @type {number}
	*/
	this.type=undefined;
	/**
	* 検索文字列。
	* @type {string|number}
	*/
	this.text=undefined;
	/**
	* 検索対象に行頭行末の空白を含める。
	* @type {bool}
	*/
	this.spaces=undefined;
}
/**
* 範囲指定に用いる座標。
* @enum {number}
*/
InsetText.RangeTypes={
	/**
	* 空白を除いた先頭位置。
	*/
	noblankStart:1,
	/**
	* 空白を除いた末尾位置。
	*/
	noblankEnd:2,
	/**
	* 選択開始位置。
	*/
	selectionStart:3,
	/**
	* 選択終了位置。
	*/
	selectionEnd:4,
	/**
	* 先頭位置。
	*/
	fullStart:5,
	/**
	* 末尾位置。
	*/
	fullEnd:6,
	/**
	* マッチ結果の開始位置。
	*/
	matchStart:7,
	/**
	* マッチ結果の終了位置。
	*/
	matchEnd:8
}
/**
* 検索タイプ。
* @enum {number}
*/
InsetText.SearchTypes={
	/**
	* 先頭にマッチする。
	*/
	head:1,
	/**
	* 末尾にマッチする。
	*/
	tail:2,
	/**
	* 選択位置の直前にマッチする。
	*/
	selectionFront:3,
	/**
	* 選択位置の直後にマッチする。
	*/
	selectionBack:4,
	/**
	* 選択位置より前の文字列に一致する。
	*/
	beforeSelection:5,
	/**
	* 選択位置より後の文字列に一致する。
	*/
	afterSelection:6,
	/**
	* 選択文字列に一致する。
	*/
	selection:7,
	/**
	* 選択位置より前の文字列の長さが一致する。
	*/
	beforeLength:8,
	/**
	* 選択位置より後の文字列の長さが一致する。
	*/
	afterLength:9,
	/**
	* 選択文字列の長さが一致する。
	*/
	selectionLength:10,
	/**
	* 選択位置より前の文字列に含まれる。
	*/
	beforeContain:11,
	/**
	* 選択位置より後の文字列に含まれる。
	*/
	afterContain:12,
	/**
	* 選択文字列に含まれる。
	*/
	selectionContain:13,
	/**
	* 選択位置より前の文字列を正規表現検索する。
	*/
	beforeRegExp:14,
	/**
	* 選択位置より後の文字列を正規表現検索する。
	*/
	afterRegExp:15,
	/**
	* 選択文字列を正規表現検索する。
	*/
	selectionRegExp:16,
	/**
	* 行全体の長さが一致する。
	*/
	lineLength:17,
	/**
	* 行全体に含まれる。
	*/
	lineContain:18,
	/**
	* 行全体を正規表現検索する。
	*/
	lineRegExp:19
}
