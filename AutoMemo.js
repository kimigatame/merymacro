#title="自動保存メモ"
#include "kili/KILI.js"

/**
* @fileoverview メモファイルを作成して常に自動保存する。
*/

var Event;
if(Event==null){
	//直接実行した場合はメモファイルを開く
	var fso=new ActiveXObject("Scripting.FileSystemObject");
	var folder=fso.BuildPath(fso.GetParentFolderName(Editor.FullName),"store");
	if(!fso.FolderExists(folder)){
		fso.CreateFolder(folder); 
	}
	var file=fso.BuildPath(folder,"memo");
	var docs=KILI.docex.all();
	var found=false;
	for(var i=0;i<docs.length;i++){
		if(docs[i].FullName==file){
			found=true;
			break;
		}
	}
	if(!found){
		var $e=GEO();
		$e.NewFile();
		if(fso.FileExists(file)){
			$e.OpenFile(file,meEncodingUTF16LEBOM,false);
		}
		else{
			var $$d=$e.ActiveDocument;
			$$d.Encoding=meEncodingUTF16LEBOM;
			$$d.Save(file);
		}
}
}
/**
* イベント用。<br />
* OnModifiedに追加する。保存処理はEvent.AutoSaveを利用している。
* @example
* //EventMain.js
* function OnModified(){
* 	AutoMemo();
* 	Event.AutoSave();
* }
*/
function AutoMemo(){
	Event.AutoSave.pattern.push("store\\\\memo");
}
